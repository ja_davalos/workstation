package com.assoc.jad.classformat.bld;

import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import org.apache.bcel.Const;
import org.apache.bcel.classfile.Attribute;
import org.apache.bcel.classfile.ClassParser;
import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.classfile.Signature;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.InstructionProcessor;
import com.assoc.jad.bytecode.utils.Utilities;

public class BldJavaSource {

	private FileInputStream fileInputStream;
	private String filename;
	private StringBuilder indentation = new StringBuilder("\t");

	public BldJavaSource(FileInputStream inpf, String filename) {
		this.fileInputStream = inpf;
		this.filename = filename;
		bldJavaSource();
	}

	private void bldJavaSource() {

		try {
	        ClassParser classParser = new ClassParser(fileInputStream, filename);
	        Utilities.javaClass = classParser.parse();
			Utilities.hashMapImports.clear();
	        setClassHeader();
	        bldFields();
	        for (Method method : Utilities.javaClass.getMethods()) {
	        	if (method.getName().startsWith("<clinit>") || method.getName().startsWith("<init>")) continue;
	        	System.out.println(System.lineSeparator()+indentation+method.toString()+" {");
	        	decompileMethod(method);
	        	printMethod();
	        }
	        tempPrintImports();
		} catch (Exception e) {e.printStackTrace();}
		System.out.println("}");
	}

	private void printMethod() {
		for (int i=0;i<Utilities.javaCode.size();i++) 
			System.out.print(Utilities.javaCode.get(i));
		System.out.println(indentation+"}");
	}

	private void bldFields() {
		HashMap<String,String> notInitFields = new HashMap<String,String>();
		Field[] fields = Utilities.javaClass.getFields();
		Utilities utils = new Utilities();

        for (Method method : Utilities.javaClass.getMethods()) {
        	if (!(method.getName().startsWith("<clinit>") || method.getName().startsWith("<init>"))) continue;

        	decompileMethod(method);
        	for (int i=0;i<Utilities.javaCode.size();i++) {
        		String wrkstr = Utilities.javaCode.get(i);
        		wrkstr = wrkstr.replaceAll("this.", "");
        		int ndx1 = wrkstr.indexOf("=");
        		if (ndx1 == -1) continue;
        		String wrkvar = wrkstr.substring(0,ndx1).trim();
        		int ndx2 = wrkvar.lastIndexOf(" ");
        		String key = wrkvar.substring(++ndx2);
        		notInitFields.put(key, wrkstr.substring(++ndx1));
        	}
        }
		for (int i=0;i<fields.length;i++) {
			utils.init_accum();
			String trimmedSignature = utils.getSignature(fields[i].getSignature());
			String modifier = resolveClassAccess(fields[i].getModifiers(),true).toString();
			Attribute[] attributes = fields[i].getAttributes();
			for (int j=0;j<attributes.length;j++) {
				if (attributes[j].getClass().getName().indexOf("Signature") == -1) continue;
				Signature signature = (Signature)attributes[j];
				trimmedSignature = utils.signatureParameterType(signature.getSignature());
			}
			if (fields[i].toString().indexOf('=') == -1) { 	//no initialized;
				String value = notInitFields.get(fields[i].getName());
				if (value == null) {
					System.out.println("\t"+modifier+" "+trimmedSignature+" "+fields[i].getName()+";");
				} else {
					int ndx = trimmedSignature.indexOf('<');
					if (ndx != -1 ) value = value.replace(trimmedSignature.substring(0,ndx), trimmedSignature);
					System.out.print("\t"+modifier+" "+trimmedSignature+" "+fields[i].getName()+" = "+value);
				}
			} else
			System.out.print("\t"+fields[i].toString()+";"+System.lineSeparator());
		}
	}

	private void tempPrintImports() {	//TODO
		Utilities.hashMapImports.keySet().stream()
		.forEach(key -> System.out.println("import "+key+";"));
	}

	private void decompileMethod(Method method) {
		
		Utilities.loopEntryToExitPoint = new HashMap<Integer, BranchAnalyzer>();
		Utilities.methodInitVars = new HashMap<String,String>();
		Stack<String> operandStack = new Stack<String>();
		Utilities.javaCode = new ArrayList<String>();
		int len = this.indentation.length();
		byte[] bytecodes = method.getCode().getCode();
		LocalVariable[] lvs = method.getLocalVariableTable().getLocalVariableTable();
		for (int i=0;i<lvs.length;i++) {
			if (lvs[i].getStartPC() != 0) continue;
			Utilities.methodInitVars.put(lvs[i].getName(), lvs[i].getName());
		}

		InstructionProcessor instructionProcessor = new InstructionProcessor(operandStack,method);
		for (int i=0;i<bytecodes.length;i++) {
			instructionProcessor.process(i,null, null);
			instructionProcessor.addJavaLine(i);
			i = instructionProcessor.getPointer();
		}
		instructionProcessor.bldEndLine();
		indentation.setLength(len);
	}
	private void setClassHeader() {
		
		int ndx = Utilities.javaClass.getClassName().lastIndexOf('.');
		StringBuilder sb = new StringBuilder();
		sb.append("package ").append(Utilities.javaClass.getClassName().substring(0, ndx)).append(';').append(System.lineSeparator());
		sb.append(resolveClassAccess(Utilities.javaClass.getAccessFlags(),false));
		sb.append(Utilities.javaClass.getClassName().substring(++ndx)).append(" ");
		if (!"java.lang.Object".equals(Utilities.javaClass.getSuperclassName()))
			sb.append(" extends ").append(Utilities.javaClass.getSuperclassName());
		System.out.println(sb.toString()+ " {");
	}

	private StringBuilder resolveClassAccess(int access,boolean isAField) {
		StringBuilder sb = new StringBuilder();
		
		if ((access & Const.ACC_PUBLIC) == Const.ACC_PUBLIC) sb.append("public ");
		if ((access & Const.ACC_FINAL) == Const.ACC_FINAL) sb.append("final ");
		if ((access & Const.ACC_INTERFACE) == Const.ACC_INTERFACE) sb.append("interface ");
		if (!isAField) if (!((access & Const.ACC_INTERFACE) == Const.ACC_INTERFACE)) sb.append("class ");
		if ((access & Const.ACC_ABSTRACT) == Const.ACC_ENUM) sb.append("abstract ");
		if ((access & Const.ACC_SYNTHETIC) == Const.ACC_ENUM) sb.append("synthetic ");
		if ((access & Const.ACC_ANNOTATION) == Const.ACC_ENUM) sb.append("annotation ");
		if ((access & Const.ACC_ENUM) == Const.ACC_ENUM) sb.append("enum ");
		if ((access & Const.ACC_STATIC) == Const.ACC_STATIC) sb.append("static ");
		
		return sb;
	}
}