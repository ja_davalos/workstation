package com.assoc.jad.tools;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;

public class WorkStationStatic {
	public static final String DEPLOYDIR 		= "C:/com.assoc.jad/backup";
	
	public static final String HOME		 		= "HOME";
	public static final String ROOTDIR	 		= "ROOTDIR";
	public static final String ARRAYLIST 		= "ARRAYLIST";
	public static final String OUTDRIVE 		= "E:";
	public static final String BACKUPS  		= "BACKUPS";
	public static final String RECOVERY 		= "RECOVERY";
	public static final String BASE     		= "_BASE";
	public static final String REMOTEPCNAME     = "remotePCName";  //always the beginning of the parameter list
	public static final String REMOTEUSER	    = "remoteUser";
	public static final String PASSWORD		    = "Password";
	public static final String CONFIGFILE	    = "configFile";
	public static final String OVERRIDEINDIR    = "overrideInDir"; //comma separated directories if more than one
	public static final String FILEFILTER		= "FILE";
	public static final String FOLDERFILTER		= "FOLDER";
	public static final String UPPERBOUND		= "UPPERBOUND";		//upper bound folder
	public static final String FULLPATH			= "FULLPATH";		
	public static final String computer    		= "computer";
	public static final String directory   		= "directory";
	public static final String look_up     		= "look up";
	public static final String Filter_Type 		= "Filter Type";
	public static final String SCRIPTJARDIR 	= "com/assoc/jad/scripts";
	public static final String EXCLUDEENDWITH 	= "excludeEndWith"; 	//comma separated
	public static final String EXCLUDEDIRECTORY	= "excludeDirectory";//comma separated directories if more than one
	
	

	public static final String[] CONFIGKEYWORDS = 
		{REMOTEPCNAME,REMOTEUSER,CONFIGFILE,OVERRIDEINDIR
		};
	
	public static String remotePCName  = "";
	public static String remoteUser    = "";
	public static String configFile    = "";
	public static String overrideInDir = "";
	public static String directory1    = "";
	public static String directory2    = "";

	public static void collectInputs(String arg) {

		arg = arg.trim();
		if (arg.length() <= 0 || arg.charAt(0) == '#') return;
		else if (arg.indexOf("remotePCName")  != -1) remotePCName  	= arg.substring(arg.indexOf("=") + 1);
		else if (arg.indexOf("remoteUser")    != -1) remoteUser  	= arg.substring(arg.indexOf("=") + 1);
		else if (arg.indexOf("overrideInDir") != -1) overrideInDir 	= arg.substring(arg.indexOf("=") + 1);
		else if (arg.indexOf("configFile")    != -1) configFile    	= arg.substring(arg.indexOf("=") + 1);
		else if (arg.indexOf("directory1")    != -1) directory1    	= arg.substring(arg.indexOf("=") + 1);
		else if (arg.indexOf("directory2")    != -1) directory2    	= arg.substring(arg.indexOf("=") + 1);
	}
	public static String replaceSpecialCharsAndSpaces(String param) {
		return param.replaceAll(":", "_").replaceAll("\\\\", "_").replaceAll("/", "_").replaceAll(" ", "_");
	}
	public static boolean writefile(String outputdir, String outputfile,String inputdir, String inputname,String rootInDir) {
		
		int ndx = inputdir.indexOf(rootInDir);
		if (ndx != -1) outputdir += inputdir.substring(ndx+rootInDir.length());
		File file = new File(inputdir+File.separator+inputname);
		Path source = Paths.get(inputdir+File.separator+inputname);
		Path target = Paths.get(outputdir+File.separator+outputfile);
		File bldDirs = new File(outputdir);
		bldDirs.mkdirs();
		try {
			if (file.isDirectory()) {
				bldDirs = new File(outputdir+File.separator+outputfile);
				bldDirs.mkdirs();
				BasicFileAttributes attributes = Files.readAttributes(source, BasicFileAttributes.class);
				FileTime creationTime  = attributes.creationTime();
				FileTime modifiedTime  = attributes.lastModifiedTime();
				Files.setAttribute(target, "creationTime", creationTime);
				Files.setAttribute(target, "lastModifiedTime", modifiedTime);
				return true;
			}
			Files.copy(source, target, StandardCopyOption.COPY_ATTRIBUTES);
		} catch (IOException e1) {
			System.out.println("could not copy file "+e1);
			e1.printStackTrace();
			return true;
		}
		return true;
	}
}