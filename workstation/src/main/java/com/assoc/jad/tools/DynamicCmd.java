package com.assoc.jad.tools;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;

import javax.swing.JOptionPane;

import com.assoc.jad.ui.CRDTools;

public class DynamicCmd implements Runnable {

	private String cmd;
	private String clazz;
	private String location;

	public DynamicCmd(String cmd, String clazz, String location) {
		this.cmd = cmd;
		this.clazz = clazz;
		this.location = location;
	}
	private boolean verifyJarFile() {
		byte[] jarTag = {0x50,0x4b,0x03,0x04};
		byte[] bytes = new byte[1024];
		FileInputStream inpf = null;
		int len = 0;
		File file = new File(location);
		boolean flag = true;
		try {
			inpf = new FileInputStream(file);
			len = inpf.read(bytes);
			if (len == -1) {
				JOptionPane.showMessageDialog(null,"failed to read file "+file.getAbsolutePath());
			} else
			for (int i=0;i<jarTag.length;i++) {
				if (jarTag[i] == bytes[i]) continue;
				flag = false;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (inpf != null ) {
				try { inpf.close(); } catch (Exception e) {};
			}
		}
		if (!flag) 
			JOptionPane.showMessageDialog(null,"file is not a ajr file name="+file.getAbsolutePath());
		return flag;
	}
	private String getUniqueClazz() {
		CRDTools crdTools = new CRDTools();
		String[] classes = crdTools.getClassNamesFromJARs(location,clazz);
		if (classes == null || classes.length == 0) {
			JOptionPane.showMessageDialog(null,"no class with partial string="+clazz+" found in file="+location);
			return null;
		}
 
		String entry = classes[0];
		if (classes.length > 1) {
			entry = (String) JOptionPane.showInputDialog(null,
					"more than one class with:\""+clazz+"\"", "select one", JOptionPane.PLAIN_MESSAGE, null,
					classes, null);
			if (entry == null) return null;
		}
		int ndx = entry.indexOf(';');
		return entry.substring(++ndx);
	}
	private void runProcess(String command) {
		Process p;
		Runtime r = Runtime.getRuntime();
		String msgInfo= "";
		String msgError= "";
		
		try {
			p = r.exec(command);
			p.waitFor();
			p.getOutputStream();
			int tmp = 0;
			while(tmp != -1) {
				tmp = p.getInputStream().read();
				if (tmp != -1) msgInfo += (char)tmp;
			}
			tmp = 0;
			while(tmp != -1) {
				tmp = p.getErrorStream().read();
				if (tmp != -1) msgError += (char)tmp;
			}
			JOptionPane.showMessageDialog(null,"error "+msgError+"\n"+"info "+msgInfo);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	private void executeCommand(Object newInstance,String fullClassName) {
		try {
			new Thread((Runnable) newInstance).start();
		} catch (Exception e) {
			runProcess("java -jar "+location+" "+fullClassName+ " 2>&1");
		}
	
	}
	private void dynamicClassLoad(String fullClassName) {
		if (fullClassName == null) return;
		File file = new File(location);
	    ClassLoader cl = null;
	    fullClassName = fullClassName.replaceAll(".class", "").replaceAll("/", ".");
		try {
		    URL url = file.toURI().toURL();
		    URL[] urls = new URL[]{url};
		    cl = new URLClassLoader(urls);
		    Class<?> cls = cl.loadClass(fullClassName);
		    executeCommand(cls.newInstance(),fullClassName);

		} catch (Exception e) {
			System.out.println("file location="+location);
			e.printStackTrace();
		}
	}
	public void run() {
		if (!verifyJarFile()) return;
		dynamicClassLoad(getUniqueClazz());
	}
}
