package com.assoc.jad.design.patterns;

public interface ICommand extends Runnable {
	public void execute();
}
