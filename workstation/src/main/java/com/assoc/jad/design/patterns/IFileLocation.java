package com.assoc.jad.design.patterns;

public interface IFileLocation {
	public void execute();
	public String getFilename();
	public String getParent();
	public String getFullLocation();
}