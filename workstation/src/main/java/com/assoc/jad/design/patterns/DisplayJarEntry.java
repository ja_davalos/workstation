package com.assoc.jad.design.patterns;

import java.util.jar.JarFile;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.assoc.jad.WorkStationDriver;
import com.assoc.jad.ui.CRDTools;

public class DisplayJarEntry implements ICommand,Runnable {

	private boolean getClassNamesFromJARs(String dir,String search) {
		CRDTools crdTools = new CRDTools();
		String[] classes = crdTools.getClassNamesFromJARs(dir,search);
		if (classes == null) {
			JOptionPane.showMessageDialog(null,"no class with string search="+search+" in jar file="+dir);
			return false;
		}
		int ndx = classes[0].indexOf(";");
		if (ndx == -1) {
			WorkStationDriver.xmlLog.append("CRDMenuTasks::ListMethodsAnyJarerror format error in entry "+classes[0]+" "+ndx);
			return false;
		}
		String entry = classes[0].substring(ndx+1);
//		if (classes.length > 1) {
			entry = (String) JOptionPane.showInputDialog(WorkStationDriver.parentFrame,
					"more than one class with:\""+search+"\"", "select one", JOptionPane.PLAIN_MESSAGE, null,
					classes, null);
			if (entry == null ) return false;
//		}

		String[] wrkstr = entry.split(";");
		WorkStationDriver.xmlLog.append(wrkstr[0]+"\n");
		WorkStationDriver.xmlLog.append(wrkstr[1]+"\n");
		WorkStationDriver.xmlLog.setCaretPosition(WorkStationDriver.xmlLog.getText().length());
		WorkStationDriver.jarFileName	       = wrkstr[0];
		WorkStationDriver.classFileName	   = wrkstr[1];
		return true;
	}
	public void execute() {
		CRDTools crdTools = new CRDTools();
		String search = JOptionPane.showInputDialog(WorkStationDriver.parentFrame,"class to search");
		if (search == null || search.length() == 0) return;

 		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("select JAR file to extract");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = chooser.showOpenDialog(WorkStationDriver.parentFrame);
		if (returnVal != JFileChooser.CANCEL_OPTION) {
			if (!getClassNamesFromJARs(chooser.getSelectedFile().getAbsolutePath(),search)) return;
			try {
				JarFile jarf = new JarFile(WorkStationDriver.jarFileName);
				WorkStationDriver.xmlLog.append((new String(crdTools.getClassFromJAR(jarf, WorkStationDriver.classFileName))));
			} catch (Exception e1) {
				WorkStationDriver.xmlLog.append("CRDMenuTasks::Display_JarEntry:"+e1.toString());
				e1.printStackTrace();
				return;
			}
		}
	}
	public void run() {
		execute();
	}

}
