package com.assoc.jad.design.patterns;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.assoc.jad.WorkStationDriver;
import com.assoc.jad.ui.CRDTools;

public class SearchClassInAllJars implements ICommand,Runnable {

	private String searchString() {
		String search = JOptionPane.showInputDialog(WorkStationDriver.parentFrame,"class to search");
		if (search == null || search.length() == 0) return null;
		return search;
	}
	private String selectDIr() {
 		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("select JAR file to extract");
		chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		int returnVal = chooser.showOpenDialog(WorkStationDriver.parentFrame);
		if (returnVal == JFileChooser.CANCEL_OPTION) return null;
		
		return chooser.getSelectedFile().getAbsolutePath();
	}
	private void getClassNamesFromJARs(String dir,String search) {
		CRDTools crdTools = new CRDTools();
		String[] classes = crdTools.getClassNamesFromJARs(dir,search);
		int ndx = classes[0].indexOf(";");
		if (ndx == -1) {
			WorkStationDriver.xmlLog.append("CRDMenuTasks::ListMethodsAnyJarerror format error in entry "+classes[0]+" "+ndx);
			return;
		}
		String entry = classes[0].substring(ndx+1);
//		if (classes.length > 1) {
			entry = (String) JOptionPane.showInputDialog(WorkStationDriver.parentFrame,
					"more than one class with:\""+search+"\"", "select one", JOptionPane.PLAIN_MESSAGE, null,
					classes, null);
			if (entry == null ) return;
//		}

		String[] wrkstr = entry.split(";");
		WorkStationDriver.xmlLog.append(wrkstr[0]+"\n");
		WorkStationDriver.xmlLog.append(wrkstr[1]+"\n");
		WorkStationDriver.xmlLog.setCaretPosition(WorkStationDriver.xmlLog.getText().length());
		WorkStationDriver.jarFileName	       = wrkstr[0];
		WorkStationDriver.classFileName	   = wrkstr[1];
	}
	public void execute() {
		String directory = this.selectDIr();
		String search = this.searchString();
		if (directory == null || search == null) return;
		
		getClassNamesFromJARs(directory,search);
	}
	public void run() {
		execute();
	}

}
