package com.assoc.jad.design.patterns;

import javax.swing.JFileChooser;

public class FileOrDirLocation implements IFileLocation {

	private String fullLocation;
	private String parent;
	private int type;
	private String filename;
	
	public FileOrDirLocation(String startloc,int type) {
		this.parent =startloc;
		this.type = type;
	}
	public void getClassFileName() {
		fullLocation = null;
		
 		JFileChooser chooser = new JFileChooser(parent);
		chooser.setDialogTitle("select file/dir");
		chooser.setFileSelectionMode(type);
		int returnVal = chooser.showOpenDialog(null);
		if (returnVal != JFileChooser.CANCEL_OPTION) {
			filename = chooser.getSelectedFile().getName();
			fullLocation = chooser.getSelectedFile().getAbsolutePath();
			parent = chooser.getSelectedFile().getParent();
		}
	}
	public void execute() {
		getClassFileName();
	}
	/*
	 * getters and setters
	 */
	public int getType() {
		return type;
	}
	public void setType(int type) {
		this.type = type;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getFullLocation() {
		return fullLocation;
	}
	public void setFullLocation(String fullLocation) {
		this.fullLocation = fullLocation;
	}
	public String getParent() {
		return parent;
	}
	public void setParent(String parent) {
		this.parent = parent;
	}

}
