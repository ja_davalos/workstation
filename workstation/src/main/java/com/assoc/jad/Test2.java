package com.assoc.jad;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.io.File;
import java.util.HashMap;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import com.assoc.jad.common.CommonGlobals;
import com.assoc.jad.design.patterns.ICommand;
import com.assoc.jad.ui.CRDPanel;
import com.assoc.jad.ui.CRDTools;
import com.assoc.jad.ui.CRDprevCopyCssJs;

public class Test2 {
	static final long serialVersionUID = 7;
    private final static String[] cmd_list = {
 //       "SearchCode SearchBuild SearchServer SearchAnyWhere",
 //       "separator",
//        "ScratchPad ScratchPadEdit TagsSearch TextSearch",
        "Misc DosWindow Telnet JavaCmd",
        "jarTools DisplayJarEntry SearchClassInAllJars classDisplayMethods DTIMap splitFile ",
        "PanelCmds DisplayJarEntries DisplayClassHex removePanel viewBinary binarySearch",
    };
    final static String[] support = {
    	"build_commands dynamicCmd",
    	"Comparefiles",
    };
/*    private final static String[] xmlLogTasks = {
        "display",
        "searchLog",
        "copyClipboardToLog",
        "clearLog",
    };
*/    
    public static HashMap<String,ICommand> commandObjs = new HashMap<String,ICommand>();
    
	public static final String local_dir   = CommonGlobals.LOCALDIR+File.separator+"wrkdir";
	public static JTextArea xmlLog         = new JTextArea(20,60);
	public static JFrame parentFrame				= new JFrame();
	public static String jarFileName	   = "";
	public static String classFileName	   = "";
	public static CRDprevCopyCssJs prevCopy= new CRDprevCopyCssJs();
	private CRDPanel crdPanel = new CRDPanel();
	StringBuilder sb = new StringBuilder();

	public void menufields() {
		String[] temps = new String[4]; 
		String temp = "CCC";
		temps[1] = "A"+"BB"+temp+"DDDD"+"EEEEE";
		temps[2] = sb.append("A").append("BB").append(temp).append("DDDD").append("EEEEE").toString();
		temps[2] = sb.append("A").append("BB").toString() +temp+ sb.append("DDDD").append("EEEEE").toString();
	}
}
