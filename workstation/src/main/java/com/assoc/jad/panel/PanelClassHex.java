package com.assoc.jad.panel;

import java.io.DataInputStream;
import java.io.File;
import java.io.ByteArrayInputStream;

import javax.swing.JFileChooser;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.assoc.jad.common.*;
import com.assoc.jad.common.disasm.*;
import com.assoc.jad.design.patterns.FileOrDirLocation;
import com.assoc.jad.design.patterns.ICommand;
import com.assoc.jad.design.patterns.IFileLocation;
import com.assoc.jad.ui.CRDPanel;
import com.assoc.jad.ui.CRDTools;

public class PanelClassHex  implements ICommand,Runnable { 
	private static String STARTDIR = null;

	public JScrollPane scrollPane;
	public boolean endOfRun = false;
	
	private JTextArea Logger;
	private CRDTools crdtools = new CRDTools();
	private CRDPanel panel;
	
	public PanelClassHex(CRDPanel panel) {	
		this.panel = panel;
	}
	private void displayClass() {
		byte[] dynClass =  null;
		IFileLocation fileDir = new FileOrDirLocation(STARTDIR,JFileChooser.FILES_ONLY);
		fileDir.execute();
		String fullname = fileDir.getFullLocation();
		STARTDIR = fileDir.getParent();
		if (fullname == null) return;
		try {
			dynClass = crdtools.readFile(new File(fullname));
			ViewBinary vb = new ViewBinary();
			String disp = vb.ViewBldBinary(dynClass);
			Logger.append(fullname+"\n");
			Logger.append(disp);
			Logger.setCaretPosition(Logger.getText().length());
			Logger.requestFocus();
			ByteArrayInputStream bais = new ByteArrayInputStream(dynClass);
			DataInputStream inFile = new DataInputStream(bais);
			Disassembler disAsm = new Disassembler(inFile);
			disAsm.disassemble();
			Logger.append(vb.ViewBldBinary(disAsm.getMagic()));
			Logger.append(vb.ViewBldBinary(disAsm.getMinorVersion()));
			Logger.append(vb.ViewBldBinary(disAsm.getMajorVersion()));
			int totalPool = disAsm.getConstantPools();
			Logger.append("CONSTANT POOL\n");
			Logger.append(vb.ViewBldBinary(disAsm.getConstantPoolLength()));
			for (disAsm.ndx=1;disAsm.ndx<totalPool;disAsm.ndx++) {
				Logger.append(vb.ViewBldBinary(disAsm.getConstantPool()));
			}
			Logger.append("ACCESS FLAG\n");
			Logger.append(vb.ViewBldBinary(disAsm.getAccessFlags()));
			Logger.append("CLASS INFO\n");
			Logger.append(disAsm.getClassInfo()+" extends ");
			Logger.append(disAsm.getSupperClassInfo()+"\n");
			Logger.append(disAsm.getInterfaces()+"\n");
			Logger.append(disAsm.getFields()+"\n");
			Logger.append(disAsm.getMethods()+"\n");
		} catch (Exception e1) {
			Logger.append("CRDMenuTasks::Display_JarEntry:"+e1.toString());
			e1.printStackTrace();
			return;
		}
	}
	public void run() {
		PanelTaskRoot panelTaskRoot = new PanelTaskRoot(panel,this.getClass().getName());
		this.Logger = panelTaskRoot.getLogger();
		this.scrollPane = panelTaskRoot.getScrollPane();
		displayClass();
	}
	public void execute() {
		run();
	}
}
