package com.assoc.jad.panel;

import com.assoc.jad.design.patterns.ICommand;
import com.assoc.jad.ui.CRDPanel;

public class PanelRemove  implements ICommand {
	private CRDPanel panel;

	public PanelRemove(CRDPanel panel) {
		this.panel = panel;
	}

	public void run() {
		PanelTaskRoot panelTaskRoot = new PanelTaskRoot(panel);
		panelTaskRoot.removePanel();
	}
	public void execute() {
		run();
	}
}
