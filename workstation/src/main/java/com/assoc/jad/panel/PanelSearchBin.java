package com.assoc.jad.panel;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.assoc.jad.WorkStationDriver;
import com.assoc.jad.common.*;
import com.assoc.jad.design.patterns.ICommand;
import com.assoc.jad.ui.CRDPanel;
import com.assoc.jad.ui.SearchBinInputs;

/**
 * will search binary file for hex value matches entered as strings
 * 
 * @author DMAJAD0
 *
 */
public class PanelSearchBin implements ICommand,Runnable {
	private static String STARTDIR = null;
	public JScrollPane scrollPane;
	public boolean endOfRun = false;

	private JTextArea logger;
	private static final int TWOMEGS = 2 * 1024 * 1024;
	private int counter = 0;

	private String startLocation = "";
	private byte[] bytesFromInputString = null;
	private int inputIndex = 0;
	private Tools tools = new Tools();
	private SearchBinInputs searchBinInputs = new SearchBinInputs();
	private String jarName = "";
	private String jarEntry = "";
	private String jarEntryWithJar  = "";
	private CRDPanel panel;

	public PanelSearchBin(CRDPanel panel) {
		this.panel = panel;
	}

	private boolean compareToInputString(byte[] bytes, int len) {
		boolean flag = false;
		if (len <= 0)
			return flag;

		for (int i = 0; i < len; i++) {
			for (; inputIndex < bytesFromInputString.length; inputIndex++) {
				if (bytes[i + inputIndex] != bytesFromInputString[inputIndex]) {
					inputIndex = 0;
					break;
				}
			}
			if (bytesFromInputString.length == inputIndex) {
				flag = true;
				inputIndex = 0;
				break;
			}
		}
		return flag;
	}

	private void readInput(InputStream is, String name) {
		byte[] bytes = new byte[TWOMEGS];
		counter++;
		try {
			while (is.available() > 0) { 
				int len = is.read(bytes);
				if (compareToInputString(bytes, len)) {
					if (jarName.length() > 0)         logger.append(jarName);
					if (jarEntryWithJar.length() > 0) logger.append("!/"+jarEntryWithJar);
					if (jarEntry.length() > 0)        logger.append("!/"+jarEntry);
					jarName = "";
					jarEntry = "";
					//jarEntryWithJar = "";
					logger.append(name + "\n");
					return;
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private void hexSearchText(File file) {
		try {
			FileInputStream fis = new FileInputStream(file);
			readInput(fis, file.getAbsolutePath());
			fis.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	private void createNewInstance(String jarlocation) {
		PanelSearchBin embededJar = new PanelSearchBin(panel);
		embededJar.bytesFromInputString = bytesFromInputString;
		embededJar.jarEntryWithJar = jarEntryWithJar;
		embededJar.getFile(jarlocation);
		counter += embededJar.counter;
		jarEntryWithJar = "";		
	}
	private boolean downloadIfJar(String jarnam,String jarentry) {
		FileOutputStream outd;
		URL url=null; 
		byte[] bytes = new byte[1024*1024];
		int len;
		
		if (jarentry.indexOf(".jar") == -1 &&
			jarentry.indexOf(".aar") == -1 &&
			jarentry.indexOf(".war") == -1) return false;
		
		jarEntryWithJar += "!/"+jarentry;
		try {
			File oFile = new File("c:\\jadtemp");
			oFile.mkdirs();
			int ndx = jarentry.lastIndexOf('/');
			String subjarfile = jarentry;
			if (ndx != -1) subjarfile = jarentry.substring(++ndx);
			oFile = new File("c:\\jadtemp"+File.separator+subjarfile);

			outd = new FileOutputStream(oFile);
			url = new URL("jar:file:"+jarnam+"!/"+jarentry);
			JarURLConnection jarConn = (JarURLConnection)url.openConnection();
			JarEntry jarexec = jarConn.getJarEntry();
			JarFile jarfile = jarConn.getJarFile();
			BufferedInputStream src_jar = new BufferedInputStream(jarfile.getInputStream(jarexec));
			while ((len = src_jar.read(bytes,0,bytes.length)) != -1) {
				outd.write(bytes,0,len);
			}
			src_jar.close();
			outd.close();
			createNewInstance(oFile.getAbsolutePath());
//			hexSearchJar(oFile,jarentry);
			oFile.delete();
			return true;
		} catch (Exception err) {
			System.out.println(url+ "CRDTabPanelSearchBin::downloadIfJar " + err.toString());
			return false;
		}
	}
	private void hexSearchJar(File file,String name) {
		JarFile jarfile = null;
		try {
			jarfile = new JarFile(file);
			Enumeration<JarEntry> jarentries = jarfile.entries();
			while (jarentries.hasMoreElements()) {
				this.jarName = name;
				JarEntry entryFile = (JarEntry) jarentries.nextElement();
				if (entryFile.isDirectory())
					continue;

				this.jarEntry = entryFile.getName();
				if (downloadIfJar(jarfile.getName(), entryFile.getName())) continue;
				
				InputStream is = jarfile.getInputStream(entryFile);
				readInput(is, "");
				is.close();
			}

		} catch (Exception e) {
			hexSearchText(file);
		} finally {
			try {
				if (jarfile != null)
					jarfile.close();
			} catch (Exception e) {
			}
		}

	}

	private void getFile(String location) {
		File file = new File(location);
		if (file.isFile()) {
			inputIndex = 0;
			hexSearchJar(file,location);
			return;
		}

		File listFiles[] = file.listFiles();
		for (int i = 0; i < listFiles.length; i++) {
			if (listFiles[i].isFile()) {
				hexSearchJar(listFiles[i],listFiles[i].getAbsolutePath());
				continue;
			}
			getFile(listFiles[i].getAbsolutePath());
		}
	}

	private boolean getStartLocation() {
		JFileChooser chooser = new JFileChooser(STARTDIR);
		chooser.setDialogTitle("select directory or file to search");
		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		int returnVal = chooser.showOpenDialog(WorkStationDriver.parentFrame);
		if (returnVal == JFileChooser.CANCEL_OPTION)
			return false;
		STARTDIR =  chooser.getSelectedFile().getParent();
		startLocation = chooser.getSelectedFile().getAbsolutePath();
		return true;
	}

	private byte[] trimLeadingZeroes(byte[] workArray) {
		int j = 0;
		for (int i=0;i<workArray.length;i++) {
			if (workArray[i] == 0x0) j++;
			else break;
		}
		byte[] newArray = new byte[workArray.length - j];
		int k=0;
		for (;j<workArray.length;j++,k++) {
			newArray[k] = workArray[j];
		}
		
		return newArray;
	}
	private void getRequiredInput() {
		bytesFromInputString = null;
		searchBinInputs.run();
		String search = searchBinInputs.getSearch().trim();
		String type = searchBinInputs.getType();
		if (search == null || search.trim().length() == 0) {
			JOptionPane.showMessageDialog(null, "Must enter a search value");
			return;
		}
		try {
			if (type.equals(SearchBinInputs.HEXSTRING)) {
				bytesFromInputString = tools.fromHexStringToByteArray(search);
			} else if (type.equals(SearchBinInputs.INT)) {
				bytesFromInputString = trimLeadingZeroes(tools.bldIntToArray(new Integer(search)));
			}
		} catch (java.lang.NumberFormatException e) {
			JOptionPane.showMessageDialog(null, "invalid number \"" + search+ "\" please try again");
			search = "";
		}

	}

	public void run() {
		PanelTaskRoot panelTaskRoot = new PanelTaskRoot(panel,this.getClass().getName());
		this.logger = panelTaskRoot.getLogger();
		this.scrollPane = panelTaskRoot.getScrollPane();

		if (!getStartLocation())
			return;
		while (bytesFromInputString == null || bytesFromInputString.length == 0)
			getRequiredInput();

		getFile(startLocation);
		logger.append("total files read="+counter + "\n");

	}

	public void execute() {
		run();
	}
}
