package com.assoc.jad.panel;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JTextArea;
import javax.swing.text.BadLocationException;

import com.assoc.jad.classformat.JavaClass;
import com.assoc.jad.common.*;
import com.assoc.jad.design.patterns.FileOrDirLocation;
import com.assoc.jad.design.patterns.ICommand;
import com.assoc.jad.design.patterns.IFileLocation;
import com.assoc.jad.ui.CRDPanel;
/*
 * will display in its own panel a binary and character representation of any file.
 */
public class PanelDisplayBin  implements ICommand {
	private static String STARTDIR = null;

	private CRDPanel panel;
	private JTextArea logger;
	private File inputFile = null;
	private int startingPoint = 0;
	private JTextArea textRect = new JTextArea();

	private JButton cursorPos;
	
	public PanelDisplayBin(CRDPanel panel) {
		this.panel = panel;
	}
	private void displayBin() {
		int ctr = 0;
		IFileLocation fileDir = new FileOrDirLocation(STARTDIR,JFileChooser.FILES_ONLY);
		fileDir.execute();
		String fullname = fileDir.getFullLocation();
		STARTDIR = fileDir.getParent();
		if (fullname == null) return;

		inputFile = new File(fullname);
		int fileSize = (int) inputFile.length();
		StringBuilder sb = new StringBuilder();
		ViewBinary vb = new ViewBinary();
		logger.append(fullname+System.lineSeparator());
		startingPoint = logger.getText().length();
		logger.addMouseListener(new MyMouseListener());
		try {
			DataInputStream dis = new DataInputStream(new FileInputStream(inputFile));
			byte[] bytes = new byte[fileSize];
			while ((ctr = dis.read(bytes,0,fileSize)) != -1) {
				sb.append(vb.ViewBldBinary(bytes,ctr));
			}
			logger.append(sb.toString());
			dis.close();
		} catch (Exception e1) {
			logger.append("CRDTabPanelDisplayBin::displayBin:"+e1.toString());
			e1.printStackTrace();
			return;
		}
	}
	private void testClassFormat() {
		try {
			JavaClass cf = new JavaClass(inputFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	public void run() {
		PanelTaskRoot panelTaskRoot = new PanelTaskRoot(panel,this.getClass().getName());
		logger = panelTaskRoot.getLogger();
		cursorPos = panelTaskRoot.getCursorPos();
		displayBin();
		testClassFormat();
	}
	public void execute() {
		run();
	}
	private void buildrect(String text,int pos) {
		Rectangle rec3 = new Rectangle();
		logger.remove(textRect);

		FontMetrics fm = logger.getFontMetrics(logger.getFont());
		textRect.setFont(logger.getFont());
		int width  = fm.charWidth('m');
		int height = fm.getHeight();

		try {
			rec3 = logger.modelToView(pos);
		} catch (BadLocationException e2) {
		}

		rec3.width  = text.length()*width;
		rec3.height = height;
		textRect.setOpaque(true); //MUST do this for background to show up.
		textRect.setBackground(Color.yellow);
		textRect.setBounds(rec3.x,rec3.y,rec3.width,rec3.height);
		textRect.setText(text);
		logger.add(textRect);
		logger.repaint();
	}
	private void displayClicked() {
		int pos = logger.getCaretPosition();
		String upToCaret = logger.getText().substring(0,pos);
		String fromCaret = logger.getText().substring(pos);
		int ndx = upToCaret.lastIndexOf(System.lineSeparator());
		ndx += System.lineSeparator().length();

		int ndx2 = fromCaret.indexOf(System.lineSeparator());
		String temp2 = logger.getText().substring(ndx, pos+ndx2);
		int len = 2;
		if (pos-ndx <= temp2.indexOf('|')) {
			ndx2 = (pos-ndx)/2+temp2.indexOf('|')+1;
			if ((pos-ndx) % 2 != 0) { // if the caret is between nibble reset to beginning of byte display
				logger.setCaretPosition(--pos);
			}
			ndx += ndx2;
			len = 1;
		} else {
			ndx2 = (pos-ndx-1)*2;
			ndx2 -= temp2.indexOf('|')*2;
			ndx += ndx2+1;
		}
		buildrect(temp2.substring(ndx2,ndx2+len),ndx);
		cursorPos.setText(String.format("%d", pos-startingPoint));

	}
	/*
	 * inner classes listeners
	 */
	class MyMouseListener implements MouseListener {

		@Override
		public void mouseClicked(MouseEvent e) {
			displayClicked();
		}
		@Override
		public void mousePressed(MouseEvent e) {
		}
		@Override
		public void mouseReleased(MouseEvent e) {			
		}
		@Override
		public void mouseEntered(MouseEvent e) {
		}
		@Override
		public void mouseExited(MouseEvent e) {
		}
	}
}
