package com.assoc.jad.panel;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;
import javax.swing.UIManager;

import com.assoc.jad.WorkStationDriver;
import com.assoc.jad.ui.CRDPanel;

public class PanelTaskRoot {
	static final long serialVersionUID = 7;

	private String tabbedName;
	private CRDPanel panel;
	private JTextArea Logger = new JTextArea(20, 60);
	private JScrollPane scrollPane = new JScrollPane(Logger);
	private Dimension dim =  new Dimension();
	private JPanel contentPane = new JPanel();
	private JButton cursorPos = new JButton("0");
	private JRadioButton searchDirection = new JRadioButton("<html>on(forward)<br/>off (back)</html>");

	public PanelTaskRoot(CRDPanel panel,String name) {
		this.tabbedName = name;
		this.panel = panel;
		this.BuildTab();
	}
	public PanelTaskRoot(CRDPanel panel) {
		this.panel = panel;
	}

	public void removePanel() {
		if (panel.tabbedPane.getSelectedIndex() > 1)
			panel.tabbedPane.removeTabAt(panel.tabbedPane.getSelectedIndex());
		else
			JOptionPane.showMessageDialog(panel.contentPane,"first 2 panels can't be removed");

	}
	private void resize() {
		Dimension tmpDim = WorkStationDriver.parentFrame.getSize();
		FontMetrics fm = WorkStationDriver.parentFrame.getFontMetrics(Logger.getFont());

		tmpDim.width  = fm.charWidth('m')*140;
		if (dim == null || (tmpDim.width != dim.width || tmpDim.height != dim.height)) {
			dim.height = (int)tmpDim.getHeight();
			dim.width = (int)tmpDim.getWidth();
			WorkStationDriver.parentFrame.setSize(dim);
			tmpDim.height -= 100;
			tmpDim.width -= 100;
			scrollPane.setPreferredSize(tmpDim);
			scrollPane.repaint();
		}
    }
	private void BuildTab() {
		Logger.removeAll();
		Logger.setMargin(new Insets(0, 2, 0, 0));
		Logger.setFont(new Font("Monospaced", Font.PLAIN, 12));
		Logger.requestFocus();
		Logger.addKeyListener(new MyKeyListener());
        JRadioButton button = new JRadioButton("resize");
        button.addMouseListener(new MyMouseAdapter());
        button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				resize();				
			}
           });
        searchDirection.setSelected(true);
        searchDirection.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				e.getSource();				
			}
           });
        searchDirection.addMouseListener(new MyMouseAdapter());
        
 		contentPane.setLayout(new GridLayout(1,0));
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        JPanel commands = new JPanel();
        commands.setLayout(new GridLayout(3,1));
        commands.add(button);
        commands.add(searchDirection);
        commands.add(cursorPos);
        splitPane.setTopComponent(commands);
        splitPane.setBottomComponent(scrollPane);
 
        splitPane.setDividerLocation(80); 
        contentPane.add(splitPane);
        
		int ndx = tabbedName.lastIndexOf('.');
		if (ndx != -1) tabbedName = tabbedName.substring(++ndx);
		panel.tabbedPane.addTab(tabbedName, contentPane);
		WorkStationDriver.parentFrame.getContentPane().add(panel.tabbedPane);
		panel.tabbedPane.setSelectedIndex(panel.tabbedPane.getTabCount()-1);
		WorkStationDriver.parentFrame.setVisible(true);
	}
	/*
	 * getters and setters
	 */
	public JTextArea getLogger() {
		return this.Logger;
	}
	public JScrollPane getScrollPane() {
		return this.scrollPane;
	}
	public JPanel getContentPane() {
		return contentPane;
	}
	public void setContentPane(JPanel contentPane) {
		this.contentPane = contentPane;
	}
	public JButton getCursorPos() {
		return cursorPos;
	}
	public void setCursorPos(JButton cursorPos) {
		this.cursorPos = cursorPos;
	}
	/*
	 * global listener -
	 */
	public class MyKeyListener implements KeyListener {
		String search;
		int pos = 0;

		@Override
		public void keyTyped(KeyEvent e) {
		}
		@Override
		public void keyPressed(KeyEvent e) {
		}
		@Override
		public void keyReleased(KeyEvent e) {
			control_mode(e.getKeyChar(), e.getKeyCode(), e.getModifiers());
			window_keys(e.getKeyChar(), e.getKeyCode(), e.getModifiers());
		}
		private void window_keys(char c, int keyCode, int modifiers) {
			if (modifiers == 2)
				return;

			switch (keyCode) {
			case KeyEvent.VK_F4:
				find();
				break;
			default:
				break;
			}
		}
		private void control_mode(char c, int keyCode, int modifiers) {
			if (modifiers != 2)
				return;
			search = null;
			switch (keyCode) {
			case 'F':
				find();
				break;
			}
		}
		private void find() {
			if (search == null) {
				search = JOptionPane.showInputDialog("Search String");
			}
			if (search != null) findString();
		}
		private void findString() {
			if (searchDirection.isSelected()) pos = Logger.getText().indexOf(search, pos);
			else {
				if (pos == -1) pos = Logger.getText().length()-1; 
				String toLookback = Logger.getText().substring(0, pos);
				pos  = toLookback.lastIndexOf(search, pos);
			}
			if (pos == -1) JOptionPane.showMessageDialog(null,"string "+search+"  not found");
			else {
				Logger.setCaretPosition(pos);
				if (searchDirection.isSelected()) pos += search.length();
			}

		}
		
	}
	public class MyMouseAdapter extends java.awt.event.MouseAdapter {
        public void mouseEntered(java.awt.event.MouseEvent evt) {
        	JRadioButton radio = (JRadioButton) evt.getSource();
        	radio.setBackground(Color.GREEN);
        }
        public void mouseExited(java.awt.event.MouseEvent evt) {
        	JRadioButton radio = (JRadioButton) evt.getSource();
        	radio.setBackground(UIManager.getColor("control"));
        }
	}
}
