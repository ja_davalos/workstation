package com.assoc.jad.panel;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.jar.JarFile;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;

import com.assoc.jad.WorkStationDriver;
import com.assoc.jad.design.patterns.FileOrDirLocation;
import com.assoc.jad.design.patterns.ICommand;
import com.assoc.jad.design.patterns.IFileLocation;
import com.assoc.jad.ui.CRDPanel;
import com.assoc.jad.ui.CRDTools;
/*
 * will display in its own panel a binary and character representation of any file.
 */
public class PanelDisplayJarEntries  implements ICommand {
	private static String STARTDIR = null;

	private CRDPanel panel;
	JTextArea logger;
	private CRDTools crdtools = new CRDTools();
	private String classname;
	private String jarname;
	
	public PanelDisplayJarEntries(CRDPanel panel) {
		this.panel = panel;
	}
	
	private void decompile() {
		int x=0;
		x += 5;
	}
	private void getClassNamesFromJARs(String dir) {
		String[] classes = crdtools.getClassNamesFromJARs(dir,"");
		int ndx = classes[0].indexOf(";");
		if (ndx == -1) {
			logger.append("CRDMenuTasks::ListMethodsAnyJarerror format error in entry "+classes[0]+" "+ndx);
			return;
		}
		String entry = classes[0].substring(ndx+1);
//		if (classes.length > 1) {
			entry = (String) JOptionPane.showInputDialog(null,
					"jar list of classes", "select one", JOptionPane.PLAIN_MESSAGE, null,
					classes, null);
			if (entry == null ) return;
//		}

		String[] wrkstr = entry.split(";");
		this.classname = wrkstr[1];
		this.jarname = wrkstr[0];
		logger.append(jarname+"\n");
		logger.append(classname+"\n");
		logger.setCaretPosition(logger.getText().length());
	}
	private void DisplayJarEntry() {
		IFileLocation fileDir = new FileOrDirLocation(STARTDIR,JFileChooser.FILES_ONLY);
		fileDir.execute();
		String fullname = fileDir.getFullLocation();
		if (fullname == null) return;
		STARTDIR = fileDir.getParent();
		
		getClassNamesFromJARs(fullname);
		try {
			JarFile jarf = new JarFile(fullname);
			logger.append((new String(crdtools.getClassFromJAR(jarf, classname))));
		} catch (Exception e1) {
			logger.append("CRDMenuTasks::Display_JarEntry:"+e1.toString());
			e1.printStackTrace();
			return;
		}
	}
	public void run() {
		PanelTaskRoot panelTaskRoot = new PanelTaskRoot(panel,this.getClass().getName());
		logger = panelTaskRoot.getLogger();
		logger.addMouseListener(new MyMouseListener());
		DisplayJarEntry();
	}
	public void execute() {
		run();
	}
/*
 * inner classes 
 */
	public class MyActionListener implements ActionListener {

	    @Override
	    public void actionPerformed(ActionEvent e) {
	        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	        Transferable contents = clipboard.getContents(null);
	        boolean hasStringText = (contents != null) && contents.isDataFlavorSupported(DataFlavor.stringFlavor);
	        if (hasStringText) {
	            try {
	                String result = (String)contents.getTransferData(DataFlavor.stringFlavor);
	                int x = 0;
	                x++;
	            } catch (Exception ex) {
	                System.out.println(ex); ex.printStackTrace();
	            }
	        }
	    }
	}	
	class MyMouseListener implements MouseListener {

		public void mouseClicked(MouseEvent e) {
	        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	        Transferable contents = clipboard.getContents(null);
	        boolean hasStringText = (contents != null) && contents.isDataFlavorSupported(DataFlavor.stringFlavor);
	        if (hasStringText) {
	            try {
	                String result = (String)contents.getTransferData(DataFlavor.stringFlavor);
	    			decompile();
	            } catch (Exception ex) {
	                System.out.println(ex); ex.printStackTrace();
	            }
	        }
			
		}
		public void mousePressed(MouseEvent e) {			
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}
	}
}
