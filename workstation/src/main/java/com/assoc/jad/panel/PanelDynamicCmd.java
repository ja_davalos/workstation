package com.assoc.jad.panel;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.assoc.jad.design.patterns.ICommand;
import com.assoc.jad.tools.DynamicCmd;
import com.assoc.jad.ui.CRDPanel;
import com.assoc.jad.ui.CRDTools;
/*
 * will display panel with three input fields command, class name, location of the jar that contains the class.
 */
public class PanelDynamicCmd  implements ICommand {
	private CRDPanel panel;
	private JPanel contentPane;
	private CRDTools crdTools = new CRDTools();
	private enum InputFields {
		Command,ClassName,JarLocation;
		String input;
		public String getInput() {
			return input;
		}
		public void setInput(String parm) {
			this.input = parm;
		}
	}
	public PanelDynamicCmd(CRDPanel panel) {
		this.panel = panel;
	}
	private void bldLine(String labeltitle,int gridy,boolean clickon) {
		JTextField jtext = new JTextField(30);
		jtext.setName(labeltitle);
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(0,30,0,0); 
		if (clickon) {
			jtext.addMouseListener(new MyMouseClick());
			jtext.addKeyListener(new MyKeyListener());
		}

		c.weightx = 0.0;
		c.gridx = 0;
		c.gridy = gridy;
		contentPane.add(new JLabel(labeltitle), c);

		c.gridx = 1;
		c.gridy = gridy;
		contentPane.add(jtext, c);

	}
	private void bldButton(String value,int gridy) {
		JButton jbutton = new JButton(value);
		jbutton.addActionListener(new MyActionListener());
		
		GridBagConstraints c = new GridBagConstraints();
		c.fill = GridBagConstraints.HORIZONTAL;
		c.insets = new Insets(10,140,0,140); 
		c.weightx = 0.0;
		c.gridx = 1;
		c.gridy = gridy;
		c.anchor = GridBagConstraints.CENTER; 
		contentPane.add(jbutton, c);
	}
	private void bldContent() {
		contentPane.removeAll();
		contentPane.setLayout(new GridBagLayout());
		
		bldLine(InputFields.Command.name(),0,false);
		bldLine(InputFields.ClassName.name(),1,false);
		bldLine(InputFields.JarLocation.name(),2,true);
		bldButton("continue",4);
		this.contentPane.repaint();

	}
	private void bldDynamicCommand() {
		String cmd = InputFields.Command.getInput();
		String clazz = InputFields.ClassName.getInput();
		String location = (InputFields.JarLocation.getInput());
		new Thread(new DynamicCmd(cmd,clazz,location),"dynamicCmd").start();

	}
	private void fndDirectory(Object jtext) {
		String fullFilename = this.crdTools.getClassFileName();
		if (fullFilename == null) return;
		
		((JTextField)jtext).setText(fullFilename);
		//JOptionPane.showMessageDialog(panel.contentPane,"first 2 panels can't be removed");

	}
	public void run() {
		PanelTaskRoot panelTaskRoot = new PanelTaskRoot(panel,this.getClass().getName());
		this.contentPane = panelTaskRoot.getContentPane();
		bldContent();
	}
	public void execute() {
		run();
	}
	/*
	 * inner classes
	 */
	public class MyActionListener implements ActionListener {

		public void actionPerformed(ActionEvent e) {
			InputFields[] inputFields = InputFields.values();
			for (int i=0;i<contentPane.getComponentCount();i++) {
				for (int j=0;j<inputFields.length;j++) {
					if (!inputFields[j].name().equals(contentPane.getComponent(i).getName())) continue;
					JTextField jtf = (JTextField) contentPane.getComponent(i);
					String input = jtf.getText();
					if (input == null || input.trim().length() == 0) {
						JOptionPane.showMessageDialog(null,"must enter data on all fields; spaces are trim from input");
						return;
					}
					inputFields[j].setInput(jtf.getText().trim());
				}
			}
			bldDynamicCommand();
		}
	}
	public class MyMouseClick implements MouseListener {

	    public void mousePressed(MouseEvent e) {
	    }
	    public void mouseReleased(MouseEvent e) {
	    }
	    public void mouseEntered(MouseEvent e) {
	    }
	    public void mouseExited(MouseEvent e) {
	    }
	    public void mouseClicked(MouseEvent e) {
	    	fndDirectory(e.getSource());
	    }
	}
	public class MyKeyListener implements KeyListener {

		public void keyTyped(KeyEvent e) {			
		}
		public void keyPressed(KeyEvent e) {
	    	fndDirectory(e.getSource());
		}
		public void keyReleased(KeyEvent e) {			
		}
	}
}
