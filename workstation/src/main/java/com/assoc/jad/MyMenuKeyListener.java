package com.assoc.jad;

import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JTextArea;

import com.assoc.jad.ui.MenuCommandsInterface;

public class MyMenuKeyListener implements KeyListener {
	private JTextArea panelLogger;
	private MenuCommandsInterface mc;
	/**
	 * this allow the listener to schedule methods that are member of the class that instantiate it.
	 * the default method list is CRDMenuTasks
	 * it looks on both methods array first in the parent then in the global CRDMenuTasks
	 * @param screenCmds
	 */
	public MyMenuKeyListener(Object screenCmds) {
		if (screenCmds == null) return;
	}

	public MyMenuKeyListener() {
	}
	public void actionPerformed(ActionEvent e) {
	}

	public void keyTyped(KeyEvent e) {
	}

	public void keyPressed(KeyEvent e) {
	}

	public void keyReleased(KeyEvent e) {
		control_mode(e.getKeyChar(), e.getKeyCode(), e.getModifiers());
		window_keys(e.getKeyChar(), e.getKeyCode(), e.getModifiers());
	}
	public void window_keys(char c, int keyCode, int modifiers) {
		if (modifiers == 2)
			return;

		switch (keyCode) {
		case KeyEvent.VK_F4:
			////DynamicMethod("CmdKeyLogFindNext",(Object)null);
			break;
		default:
			break;
		}
	}
	public void control_mode(char c, int keyCode, int modifiers) {
		if (modifiers != 2)
			return;
		switch (keyCode) {
		case 'F':
			////DynamicMethod("CmdKeyLogFindNext",(Object)null);
			break;
		case KeyEvent.VK_PAGE_DOWN:
			//DynamicMethod("Down",(Object)null);
			break;
		case KeyEvent.VK_PAGE_UP:
			//DynamicMethod("Up",(Object)null);
			break;
		case KeyEvent.VK_HOME:
			//DynamicMethod("Top",(Object)null);
			break;
		case KeyEvent.VK_END:
			//DynamicMethod("Bottom",(Object)null);
			break;
		}
	}
	public JTextArea getPanelLogger() {
		return panelLogger;
	}
	public void setPanelLogger(JTextArea panelLogger) {
		this.panelLogger = panelLogger;
	}
	public MenuCommandsInterface getMc() {
		return mc;
	}
	public void setMc(MenuCommandsInterface mc) {
		this.mc = mc;
	}
}
