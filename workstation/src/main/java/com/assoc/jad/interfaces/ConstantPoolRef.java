package com.assoc.jad.interfaces;

import org.apache.bcel.classfile.ConstantFieldref;
import org.apache.bcel.classfile.ConstantNameAndType;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.ConstantUtf8;
import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.Utilities;

public abstract class ConstantPoolRef {
	
	public byte getTag(Method method,int ndx) {
		byte[] codes = method.getCode().getCode();
		return codes[ndx+1];
	}
	public int getClassIndex(Method method,int ndx) {
		byte[] codes = method.getCode().getCode();
		int cpIndex = codes[ndx+2] << 8 |  codes[ndx+3];
		return cpIndex;
	}
	public String getRefName(Method method,int ndx) {
		Utilities utils = new Utilities();
		int cpIndex =  utils.getIndexFromCode(method, ndx,2);
		ConstantPool constantPool = method.getConstantPool();	
		ConstantFieldref fieldRef = (ConstantFieldref) constantPool.getConstant(cpIndex);
		cpIndex = fieldRef.getNameAndTypeIndex();
		ConstantNameAndType nameTypeRef = (ConstantNameAndType) constantPool.getConstant(cpIndex);
		ConstantUtf8 objUtf8 = (ConstantUtf8)constantPool.getConstant(nameTypeRef.getNameIndex());
		
		return objUtf8.getBytes();
	}
}
