package com.assoc.jad.interfaces;

import java.util.Stack;

import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;

public interface IInstructions {
	String execute(Method method,int ndx, Stack<String> operandStack,BranchAnalyzer parentBranch);
	int getDisplacement();
	String getOutputLine();
	String getCondition();
	String getFormatString();
}
