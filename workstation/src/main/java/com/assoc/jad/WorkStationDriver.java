package com.assoc.jad;

import javax.swing.*;

import com.assoc.jad.common.CommonGlobals;
import com.assoc.jad.design.patterns.DisplayJarEntry;
import com.assoc.jad.design.patterns.ICommand;
import com.assoc.jad.design.patterns.SearchClassInAllJars;
import com.assoc.jad.panel.PanelClassHex;
import com.assoc.jad.panel.PanelDisplayBin;
import com.assoc.jad.panel.PanelDisplayJarEntries;
import com.assoc.jad.panel.PanelDynamicCmd;
import com.assoc.jad.panel.PanelRemove;
import com.assoc.jad.ui.CRDPanel;
import com.assoc.jad.ui.CRDTools;
import com.assoc.jad.ui.CRDprevCopyCssJs;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GraphicsEnvironment;
import java.awt.Insets;
import java.io.File;
import java.util.HashMap;

public class WorkStationDriver implements Runnable{
	static final long serialVersionUID = 7;
    private final static String[] cmd_list = {
 //       "SearchCode SearchBuild SearchServer SearchAnyWhere",
 //       "separator",
//        "ScratchPad ScratchPadEdit TagsSearch TextSearch",
        "Misc DosWindow Telnet JavaCmd",
        "jarTools DisplayJarEntry SearchClassInAllJars classDisplayMethods DTIMap splitFile ",
        "PanelCmds DisplayJarEntries DisplayClassHex removePanel viewBinary binarySearch",
    };
    final static String[] support = {
    	"build_commands dynamicCmd",
    	"Comparefiles",
    };
/*    private final static String[] xmlLogTasks = {
        "display",
        "searchLog",
        "copyClipboardToLog",
        "clearLog",
    };
*/    
    public static HashMap<String,ICommand> commandObjs = new HashMap<String,ICommand>();
    
	public static final String local_dir   = CommonGlobals.LOCALDIR+File.separator+"wrkdir";
	private static final String local_docs  = CommonGlobals.LOCALDIR+File.separator+"wrkdocs";
	public static JTextArea xmlLog         = new JTextArea(20,60);
	public static JFrame parentFrame				= new JFrame();
	private static Font oldFont             = xmlLog.getFont();
	public static String jarFileName	   = "";
	public static String classFileName	   = "";
	public static CRDprevCopyCssJs prevCopy= new CRDprevCopyCssJs();

    private String selectedText;
	private CRDPanel crdPanel = new CRDPanel();
	private Dimension dim = null;
	private JScrollPane scrollPane=null;
	private JTextField miniArea      = new JTextField(60);
	private CRDTools crdtools = new CRDTools();
	private MyMenuKeyListener listener = new MyMenuKeyListener();

    private void loggerTab() {

    	xmlLog         = new JTextArea(20,60);
    	xmlLog.removeAll();
        miniArea.setEditable(false);
    	xmlLog.setMargin(new Insets(0,2,0,0));
		listener.setPanelLogger(xmlLog);
    	xmlLog.addKeyListener(listener);
        scrollPane = new JScrollPane(xmlLog);
        xmlLog.requestFocus();
		crdPanel.contentPane.add(scrollPane);
		crdPanel.contentPane.add(miniArea);
		crdPanel.tabbedPane.addTab("logger",crdPanel.contentPane);
		parentFrame.getContentPane().add(crdPanel.tabbedPane);
    }
	private void menufields() {
        GraphicsEnvironment gEnv = GraphicsEnvironment.getLocalGraphicsEnvironment();
        String envfonts[] = gEnv.getAvailableFontFamilyNames();
		int wrk_ndx = (envfonts.length/15);
		String[] cmd_font = new String[2+wrk_ndx+1];
		cmd_font[0] = "Size 10 12 14 16 18";
		cmd_font[1] = "styles PLAIN BOLD ITALIC BOLD_&_ITALIC";
		wrk_ndx = 2;
		cmd_font[wrk_ndx] = "Fonts ";
		for ( int i = 1; i < envfonts.length; i++ ) {
			if ((int)Math.IEEEremainder((double)i,15.0) == 0 ) {
				wrk_ndx++;
				cmd_font[wrk_ndx] = "Fonts ";
			}
            cmd_font[wrk_ndx] += envfonts[i].replace(' ','_')+" ";
        }

		StringBuffer wrkbuf = new StringBuffer("");
		crdPanel.reqlabel  = new String[4];
		crdPanel.reqitems  = new String[4];
		crdPanel.reqlabel[0] = "font";
		for (int i = 0;i<cmd_font.length;i++) wrkbuf.append(cmd_font[i]).append("\n");
		crdPanel.reqitems[0] = wrkbuf.toString();
		
		wrkbuf.setLength(0);
		crdPanel.reqlabel[1] = "Commands";
		for (int i = 0;i<cmd_list.length;i++) wrkbuf.append(cmd_list[i]).append("\n");
		crdPanel.reqitems[1] = wrkbuf.toString();
		
		wrkbuf.setLength(0);
		crdPanel.reqlabel[2] = "SupportAid";
		for (int i = 0;i<support.length;i++) wrkbuf.append(support[i]).append("\n");
		crdPanel.reqitems[2] = wrkbuf.toString();


		wrkbuf.setLength(0);
		//verifyDoc();
		String[] tags = crdtools.readErrorTags(WorkStationDriver.local_docs + File.separator + System.getProperty("user.name"));
		wrkbuf.append("help_html\n").append("helpScratchPad ");
		for (int i = 0;i<tags.length;i++) wrkbuf.append(tags[i]).append(" ");
		wrkbuf.append("\n");
		crdPanel.reqlabel[3] = "Help";
		crdPanel.reqitems[3] = wrkbuf.toString();
		crdPanel.run();
	}

	private void checksize() {
		Dimension tmpDim = null;
		tmpDim = parentFrame.getSize();
		tmpDim.width  -= 10;
		tmpDim.height -= 85;
		if (dim == null || (tmpDim.width != dim.width || tmpDim.height != dim.height)) {
			dim = tmpDim;
			scrollPane.setPreferredSize(dim);
		}
		String wrkstr = "info ";
        miniArea.setText(wrkstr);
		FontMetrics fm = parentFrame.getFontMetrics(oldFont);
		int width = fm.charWidth('m');
        miniArea.setColumns(dim.width/width);
        miniArea.setBackground(Color.yellow);
	}
	private void init() {
		Dimension initDim = new Dimension(850,400);
		parentFrame.setSize(initDim);
		commandObjs.put("SearchClassInAllJars",new SearchClassInAllJars());
		commandObjs.put("DisplayJarEntry",new DisplayJarEntry());
		commandObjs.put("viewBinary",new PanelDisplayBin(crdPanel));
		commandObjs.put("removePanel",new PanelRemove(crdPanel));
		commandObjs.put("DisplayClassHex",new PanelClassHex(crdPanel));
		commandObjs.put("dynamicCmd",new PanelDynamicCmd(crdPanel));
		commandObjs.put("DisplayJarEntries",new PanelDisplayJarEntries(crdPanel));
		
	}
	public synchronized void run() {
		init();
		menufields();
		loggerTab();
		checksize();
		parentFrame.setVisible(true);
	}
/*
 * getters and setters 
 */
	public void setSelectedText(String selectedText) {
		this.selectedText = selectedText;
	}
	public String getSelectedText(String selectedText) {
		return this.selectedText;
	}
}
