package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.interfaces.IInstructions;

public class IAnd implements IInstructions {
	private int disp = 0;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx, Stack<String> operandStack, BranchAnalyzer parentBranch) {

		result.setLength(0);
		Integer INTEGER = new Integer(operandStack.pop());
		String valueHex = Integer.toHexString(INTEGER);
		String value2 = operandStack.pop();
		result.append("(").append(value2).append(" & 0x").append(valueHex).append(")");
		operandStack.push(result.toString());
		result.setLength(0);
		return null;
	}

	@Override
	public int getDisplacement() {
		// TODO Auto-generated method stub
		return disp;
	}

	@Override
	public String getOutputLine() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public String getCondition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}

}
