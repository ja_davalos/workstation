package com.assoc.jad.bytecode;

import java.util.Stack;


import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.interfaces.IInstructions;

/**
 * <b>bipush Push byte</b><br/>
 * The immediate byte is sign-extended to an int value. That value is pushed onto the operand stack.
 * @author jorge
 *
 */
public class ByteIntPush implements IInstructions {
	private int disp = 1;

	@Override
	public String execute(Method method, int ndx,  Stack<String> operandStack,BranchAnalyzer parentBranch) {
		int intConst = method.getCode().getCode()[ndx+1]; //extends sign
		Integer INTCONST = new Integer(intConst);
		operandStack.push(INTCONST.toString());
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		// TODO Auto-generated method stub
		return "";
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}

}
