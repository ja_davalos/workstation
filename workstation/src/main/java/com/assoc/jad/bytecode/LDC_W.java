package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.ConstantFieldref;
import org.apache.bcel.classfile.ConstantNameAndType;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.ConstantString;
import org.apache.bcel.classfile.ConstantUtf8;
import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.interfaces.IInstructions;

public class LDC_W implements IInstructions {
	private int disp = 2;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx, Stack<String> operandStack, BranchAnalyzer parentBranch) {
		result.setLength(0);

		Utilities utils = new Utilities();
		int cpIndex =  utils.getIndexFromCode(method, ndx,disp);
		ConstantPool constantPool = method.getConstantPool();
		ConstantString constantString = (ConstantString)constantPool.getConstant(cpIndex);
		ConstantUtf8 constantUtf8 = (ConstantUtf8)constantPool.getConstant(constantString.getStringIndex());
		result.append(constantUtf8.getBytes());

/*		ConstantFieldref constantFieldref = (ConstantFieldref)constantPool.getConstant(cpIndex);
		int ndx2 = constantFieldref.getNameAndTypeIndex();
		ConstantNameAndType constantNameAndType = (ConstantNameAndType)constantPool.getConstant(ndx2);
		String fieldname = constantNameAndType.getName(constantPool);
		if (operandStack.isEmpty()) return null;
		
		if (operandStack.size() == 1) {
			String wrkstr = constantNameAndType.getSignature(constantPool).substring(1);
			if (wrkstr.charAt(wrkstr.length()-1) == ';') wrkstr = wrkstr.substring(0,wrkstr.length()-1);
			result.append(utils.trim(wrkstr)).append(" ").append(fieldname).append(" = ").append(operandStack.pop()).append(";");
			return null;
		}
		result.append(operandStack.elementAt(0)).append(" ").append(fieldname).append(" = {");
		for (int i=2;i<operandStack.size();i++,i++) {
			result.append(operandStack.elementAt(i)).append(",");
		}
		result.setLength(result.length()-1);
		if (result.charAt(result.length()-1) == '{') result.append(" };");
		else result.append(";");
		operandStack.removeAllElements();*/

		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}

	@Override
	public String getOutputLine() {
		return result.toString();
	}

	@Override
	public String getCondition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}

}
