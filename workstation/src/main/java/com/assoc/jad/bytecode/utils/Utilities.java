package com.assoc.jad.bytecode.utils;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Stack;

import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.JavaClass;
import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.Method;

public class Utilities {
	public static HashMap<String,String> methodInitVars = new HashMap<String,String>();
	public static HashMap<String,String> hashMapImports = new HashMap<String,String>();
	public static HashMap<Integer,BranchAnalyzer> loopEntryToExitPoint = new HashMap<Integer,BranchAnalyzer>();
	public static ArrayList<String> javaCode = new ArrayList<String>();
	public static Boolean ADDLINENUMBER = false;
	public static int EOCODE = -1;
	public static JavaClass javaClass;
	
	private String wrkstr = "";

	public LocalVariable selectLocalVariable(Method method,int lvindex,int pc) {
		LocalVariable[] lvs = method.getLocalVariableTable().getLocalVariableTable();
		LocalVariable lv = null;
		for (int i=0;i<lvs.length;i++) {
			if (lvs[i].getIndex() != lvindex) continue;
			int lvScope = lvs[i].getStartPC()+lvs[i].getLength();
			if (pc > lvScope) continue;
			lv = lvs[i];
			break;
		}
		return lv;
	}
	public LocalVariable localVariableByName(Method method,String name,int pc) {
		LocalVariable[] lvs = method.getLocalVariableTable().getLocalVariableTable();
		String[] words = name.split("\\W+");
		String key = words[0];
		LocalVariable lv = null;
		for (int i=0;i<lvs.length;i++) {
			if (!lvs[i].getName().equals(name)) continue;
			int lvScope = lvs[i].getStartPC()+lvs[i].getLength();
			if (pc > lvScope) continue;
			lv = lvs[i];
			break;
		}
		return lv;
	}
	public StringBuilder BldLocalVariableText(int instrLoc,LocalVariable lv,Stack<String> operandStack) {
		StringBuilder result = new StringBuilder();
		result.setLength(0);
		if (operandStack.isEmpty()) return result;
		
		init_accum();
		if (methodInitVars.get(lv.getName()) == null || (lv.getStartPC() >instrLoc && lv.getLength()+lv.getStartPC() < instrLoc ))
			if (!checkFields(lv.getName())) result.append(getSignature(lv.getSignature())).append(" ");

		result.append(lv.getName()).append(" = ").append(operandStack.pop()).append(';');
		methodInitVars.put(lv.getName(), result.toString());
		return result;
	}
	private boolean checkFields(String name) {
		Field[] fields = javaClass.getFields();
		for (int i=0;i<fields.length;i++) {
				if (fields[i].getName().equals(name)) return true;
		}
		return false;
	}

	public int getIndexFromCode(Method method,int ndx,int codeGet) {
		byte[] wrk = new byte[2];
		byte[] codes = method.getCode().getCode();
		
		wrk[0] = 0;
		if (codeGet > 1) wrk[0] = codes[++ndx];
		wrk[1] = codes[++ndx];
		int cpIndex =  ByteBuffer.wrap(wrk).getShort() & 0x0000ffff;
		return cpIndex;
	}
	private String _accum = "";
	public String getSignature(String symbol) {
		String name = symbol.substring(0, 1);
		String type = "";
		switch (name) {
			case "B":	type = "byte";break;
			case "C":	type = "char";break;
			case "D":	type = "double";break;
			case "F":	type = "float";break;
			case "I":	type = "int";break;
			case "J":	type = "long";break;
			case "S":	type = "short";break;
			case "Z":	type = "boolean";break;
			case "[":
				_accum += "[]";
				getSignature(symbol.substring(1));
				type += _accum;
				break;
			case "L":	_accum = trim(symbol.substring(1))+_accum;type = _accum;break; 
		}
		return type;
	}
	public void init_accum() {
		_accum = "";
	}
	/*
	 * getters and setters
	 */
	public String trim(String name) {
		int ndx = name.length();
		if (name.endsWith(";")) name = name.substring(0,name.length()-1);
		String saved =  name;
		ndx = name.lastIndexOf("/");
		if (ndx == -1) ndx = name.lastIndexOf(".");
		if (ndx != -1) name = name.substring(++ndx);
		if (name.endsWith(";")) ndx = name.length()-1;
		if (saved.indexOf("java/lang") == -1 && saved.indexOf('/') != -1 && saved.indexOf(';') == -1) {
			String key = saved.replace('/', '.');
			Utilities.hashMapImports.put(key, key);
		}
		return name;
	}
//		uses recursion to resolve more complex types
	public String signatureParameterType(String signature) {
		int ndx = signature.indexOf('<');
		if (ndx == -1) {
			wrkstr = "";
			return signature;
		}
		
		String clazz = "";
		String type = "";
		if (ndx != -1) {
			clazz = trim(signature.substring(1,ndx));
			type = signature.substring(++ndx);
			signatureParameterType(type);
		}
		String[] types = type.split(";");
		if (wrkstr.length() == 0) wrkstr = clazz+"<"+trim(types[0].substring(1))+","+trim(types[1].substring(1))+wrkstr+">";
		else wrkstr = clazz+"<"+trim(types[0].substring(1))+","+wrkstr+">";
		return wrkstr;
	}
	public String bldCondition(Stack<String> operandStack,String cmp) {
		StringBuilder condition = new StringBuilder();
		String symbol = cmp;
		switch (cmp) {
		case "eq":
			symbol = " != ";
			break; // the oppositite but it makes sence when you look at the code
		case "ne":
			symbol = " == ";
			break;
		case "lt":
			symbol = " < ";
			break;
		case "ge":
			symbol = " >= ";
			break;
		case "gt":
			symbol = " > ";
			break;
		case "le":
			symbol = " <= ";
			break;
		}
		String value = operandStack.pop();
		String var = operandStack.pop();
		condition.append(var + symbol + value);
		return condition.toString();
	}
	//(CC)Ljava/lang/String;
	public String methodOperands(Stack<String> operandStack,String signature) {
		StringBuilder methodParms = new StringBuilder("()");
		int ndx = signature.indexOf(')');
		if (!signature.startsWith("(") || ndx == -1) return methodParms.toString();

		methodParms.setLength(0);
		methodParms.append('(');
		String parms = signature.substring(1, ndx);
		int start = operandStack.size() - countParms(parms);
		for (int i=0;i<parms.length();i++) {
			switch (parms.charAt(i)) {
				case 'B':	methodParms.append("BYTE");break;
				case 'C':	typeCharacter(operandStack.remove(start),methodParms);break;
				case 'Z':	typeBoolean(operandStack.remove(start),methodParms);break;
				case 'L':	i += parms.substring(i).indexOf(';');typeInstance(operandStack.remove(start),methodParms);break; 
/*				case 'D':	type = "double";break;
				case 'F':	type = "float";break;
				case 'I':	type = "int";break;
				case 'J':	type = "long";break;
				case 'S':	type = "short";break;
				case 'Z':	type = "boolean";break;
				case '[':	type += _accum;break;
				case 'L':	type = _accum;break; */
			}
		}
		int len = methodParms.length()-1;
		if (methodParms.charAt(len) == ',') methodParms.setLength(len);
		methodParms.append(')');
		return methodParms.toString();
	}
	private int countParms(String parms) {
		int total = 0;
		for (int i=0;i<parms.length();i++) {
			switch (parms.charAt(i)) {
				case 'B':
				case 'C':	
				case 'D':
				case 'F':	
				case 'I':	
				case 'J':	
				case 'S':
				case 'Z':
				case '[': 	total++;break;
				case 'L':	i += parms.substring(i).indexOf(';');total++;break; 
			}
		}
		return total;
	}
	private void typeInstance(String pop, StringBuilder methodParms) {
		methodParms.append(pop).append(',');
	}
	private void typeCharacter(String value,StringBuilder methodParms) {
		Integer INTEGER = new Integer(value);
		char parm = (char)INTEGER.intValue();
		methodParms.append("'").append(parm).append("'").append(',');
	}
	private void typeBoolean(String value,StringBuilder methodParms) {
		Integer INTEGER = new Integer(value);
		String parm = INTEGER.intValue() == 0 ? "false" : "true";
		methodParms.append(parm);
	}
	public Field getAField(String fieldname) {
		Field[] fields = Utilities.javaClass.getFields();
		String[] name = fieldname.replace("this.", "").split("\\W+");
		for (int i=0;i<fields.length;i++) {
			if (fields[i].getName().equals(name[0])) return fields[i];
		}
		return null;
	}
	public StringBuilder aAStore(Method method,Stack<String> operandStack,int pc) {
		StringBuilder sb = new StringBuilder();
		if (operandStack.isEmpty()) return sb;
		
		LocalVariable lv = localVariableByName(method, operandStack.firstElement(), pc);
		if (lv == null) return sb;

		String var = operandStack.get(0);
		String index = operandStack.get(1);
		sb.append(var).append("[").append(index).append("] = ");

		String operation = " + ";
		for (int i=2;i<operandStack.size();i++) {
			String stackValue = operandStack.get(i);
			if (stackValue.lastIndexOf("new StringBuilder") != -1) continue;
			if (stackValue.charAt(0) == '"') sb.append(stackValue).append(operation);
			else {
				LocalVariable lvParm = localVariableByName(method, stackValue, pc);
				
			}
		}
		int len = sb.length()-3;
		sb.setLength(len);
		sb.append(';');
		return sb;
	}

}
