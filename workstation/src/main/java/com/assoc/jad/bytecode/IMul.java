package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.interfaces.IInstructions;

public class IMul implements IInstructions {
	private int disp = 0;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx, Stack<String> operandStack, BranchAnalyzer parentBranch) {
		result.setLength(0);
		String value1 = operandStack.pop();
		String value2 = operandStack.pop();
		result.append("(").append(value2).append(" * ").append(value1).append(")");
		operandStack.push(result.toString());
		result.setLength(0);
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		return null;
	}

}
