package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.interfaces.IInstructions;
/**
 * <b>aload index</b>
 * The index is an unsigned byte that must be an index into the local variable array of the current frame (§2.6). 
 * The local variable at index must contain a reference. The objectref in the local variable at index is pushed onto the operand stack.
 * 
 * jorge
 *
 */
public class ALoad implements IInstructions {
	private int disp = 1;

	@Override
	public String execute(Method method, int ndx, Stack<String> operandStack, BranchAnalyzer parentBranch) {
		Utilities utils = new Utilities();
		int lvIndex =  utils.getIndexFromCode(method, ndx,disp);
		LocalVariable lv = utils.selectLocalVariable(method, lvIndex, ndx);
		operandStack.push(lv.getName());
		return null;
	}

	@Override
	public int getDisplacement() {
		// TODO Auto-generated method stub
		return disp;
	}

	@Override
	public String getOutputLine() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public String getCondition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}

}
