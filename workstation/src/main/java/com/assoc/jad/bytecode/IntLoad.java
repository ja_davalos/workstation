package com.assoc.jad.bytecode;

import java.util.Stack;


import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.interfaces.IInstructions;

public class IntLoad implements IInstructions {
	private int disp = 1;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx,  Stack<String> operandStack,BranchAnalyzer parentBranch) {
		result.setLength(0);
		Utilities utils = new Utilities();
		int lvIndex =  utils.getIndexFromCode(method, ndx,disp);
		LocalVariable lv = method.getLocalVariableTable().getLocalVariableTable()[lvIndex]; 
		operandStack.push(lv.getName());
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}

}
