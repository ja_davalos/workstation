package com.assoc.jad.bytecode;

import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.bcel.classfile.ConstantFieldref;
import org.apache.bcel.classfile.ConstantNameAndType;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.interfaces.IInstructions;

public class PutStatic implements IInstructions {
	private int disp = 2;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx, Stack<String> operandStack, BranchAnalyzer parentBranch) {
		result.setLength(0);
		boolean isArray = false;
		
		Utilities utils = new Utilities();
		utils.init_accum();
		int cpIndex =  utils.getIndexFromCode(method, ndx,disp);
		ConstantPool constantPool = method.getConstantPool();
		ConstantFieldref constantFieldref = (ConstantFieldref)constantPool.getConstant(cpIndex);
		int ndx2 = constantFieldref.getNameAndTypeIndex();
		ConstantNameAndType constantNameAndType = (ConstantNameAndType)constantPool.getConstant(ndx2);
		String fieldname = constantNameAndType.getName(constantPool);
		String signature = utils.getSignature(constantNameAndType.getSignature(constantPool));
		if (operandStack.isEmpty()) return null;
		if (constantNameAndType.getSignature(constantPool).startsWith("[")) isArray = true;
		if (operandStack.size() == 1) {
			String wrkstr = constantNameAndType.getSignature(constantPool).substring(1);
			if (wrkstr.charAt(wrkstr.length()-1) == ';') wrkstr = wrkstr.substring(0,wrkstr.length()-1);
			result.append(utils.trim(wrkstr)).append(" ").append(fieldname).append(" = ").append(operandStack.pop()).append(";");
			return null;
		}
		String type = ResolveFieldType(operandStack.elementAt(0),signature);
		result.append(type).append(" ").append(fieldname).append(" = ");
		if (isArray) result.append('{');
		for (int i=1;i<operandStack.size();i++) {
			if (isArray) i++;
			result.append(operandStack.elementAt(i));
			if (!type.equals("String")) result.append(",");
			else  result.append("+");
		}
		result.setLength(result.length()-1);
		if (isArray) result.append('}');
		result.append(";");
		operandStack.removeAllElements();

		return null;
	}
	public String ResolveFieldType(String cpSignature,String fieldsignature) {
		String pattern = "(\\w+)\\W+(\\w+)\\W+(\\w+)";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(cpSignature);
		if (m.find()) {
			if (m.group(2).equals(fieldsignature)) return cpSignature;
		}
		      return fieldsignature;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		return null;
	}

}
