package com.assoc.jad.bytecode.utils;

import java.util.Stack;

import org.apache.bcel.classfile.Code;
import org.apache.bcel.classfile.CodeException;
import org.apache.bcel.classfile.ConstantClass;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer.LoopType;

public class TryCatch {

	private Method method;
	private int pc;
	private String exception;
	private boolean tryCatchBlock = false;
	private StringBuilder wrkJavaCode = new StringBuilder();
	private enum TryState {
		Try,Catch,CatchParm,CatchEnd;
	}
	private TryState state = TryState.Try;
	public TryCatch(Method method) {
		this.method = method;
	}
	public void codeException(int ndx) {

		pc = ndx;
		wrkJavaCode.setLength(0);
		switch (state) {
			case Try:
				checkTry(ndx);
				break;
			case Catch:
				checkCatch(ndx);
				break;
			case CatchParm:
				closeParmClause();
				state = TryState.CatchEnd;
			case CatchEnd:
			default:
				break;
		}
	}
	private void closeParmClause() {
		wrkJavaCode.append(") {").append(System.lineSeparator());					
		state = TryState.Try;	
		tryCatchBlock = false;
	}
	private void checkCatch(int ndx) {		
		Code code = method.getCode();
		CodeException[] codeExceptions = code.getExceptionTable();
		for (int i=0;i<codeExceptions.length;i++) {
			if (!(codeExceptions[i].getEndPC() == ndx)) continue;
			bldCatchBlock(ndx,codeExceptions[i]);
			state = TryState.CatchParm;
			break;
		}
	}
	private void bldCatchBlock(int ndx,CodeException codeException) {
		getCatchUpperLimit(ndx);
		Utilities utils = new Utilities();
		int ndx2 = codeException.getCatchType();
		ConstantPool constantPool = method.getConstantPool();
		ConstantClass constantClass = (ConstantClass) constantPool.getConstant(ndx2);
		exception = utils.trim(constantClass.getBytes(constantPool));
		pc = codeException.getHandlerPC();
		wrkJavaCode.append(System.lineSeparator()).append("} catch "+"(");					
	}
	/*
	 * for try/catch block the next instruction is a goto . we need to get to instruction pointed by goto. to end the catch block.
	 */
	private void getCatchUpperLimit(int ndx) {
		BranchAnalyzer tryCatchBranch = new BranchAnalyzer(ndx);
		InstructionProcessor instructionProcessor = new InstructionProcessor(new Stack<String>(), method);	//TODO
		tryCatchBranch.setLoopType(LoopType.TRYCATCH);
		instructionProcessor.process(ndx,null, tryCatchBranch);
		int endPC = tryCatchBranch.getEndPC();
		tryCatchBranch = new BranchAnalyzer(endPC);
		tryCatchBranch.setLoopType(LoopType.TRYCATCH);
		tryCatchBranch.setEndPC(endPC);
		Utilities.loopEntryToExitPoint.put(endPC,tryCatchBranch);
	}
	private void checkTry(int ndx) {
		Code code = method.getCode();
		CodeException[] codeExceptions = code.getExceptionTable();
		for (int i=0;i<codeExceptions.length;i++) {
			if (!(codeExceptions[i].getStartPC() == ndx)) continue;
			wrkJavaCode.append(System.lineSeparator()).append("try {");
			tryCatchBlock = true;
			state = TryState.Catch;
		}		
	}
/*
 * getters and setters 
 */
	public int getPc() {
		return pc;
	}
	public void setPc(int pc) {
		this.pc = pc;
	}
	public String getException() {
		return exception;
	}
	public void setException(String exception) {
		this.exception = exception;
	}
	public boolean isTryCatchBlock() {
		return tryCatchBlock;
	}
	public void setTryCatchFlag(boolean tryCatchBlock) {
		this.tryCatchBlock = tryCatchBlock;
	}
	public String getJavaCode() {
		return this.wrkJavaCode.toString();
	}
}
