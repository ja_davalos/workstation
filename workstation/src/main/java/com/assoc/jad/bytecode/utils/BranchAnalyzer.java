package com.assoc.jad.bytecode.utils;

public class BranchAnalyzer {

	public enum LoopType2 {
		FOR,WHILE,IFTHEN,IFTHENELSE,SWITCH,TRYCATCH,UNDEFINED
	}
	public enum LoopType {
		IFTHEN(new String[]{"","}"}),
		IFTHENELSE(new String[]{" else {","}"}),
		FOR(new String[]{"",""}),
		WHILE(new String[]{"",""}),
		SWITCH(new String[]{"switch (%s) {","}"}),
		TRYCATCH(new String[]{"try {"," }catch (%s) {"}),
		UNDEFINED(new String[]{"",""});
		
		private String[] codes;
		
		LoopType(String[] codes) {
			this.codes = codes;
		}
		public String getNextCode(int ndx) {
			if (ndx >= codes.length) return "enum::LoopType index out of range";
			return codes[ndx];
		}
	}

	private int startPC=0;
	private int endPC=0;
	private int pc;
	private LoopType loopType;
	private String condition;
	private String conditionVarName;
	private BranchAnalyzer nextBranch;
	private int counter=0;
	private int jump = 0;
	private boolean closed = false;
	
	public BranchAnalyzer(int pc) {
		this.pc = pc;
		this.loopType = LoopType.UNDEFINED;
	}
/*
 * getter and setters
 */
	public int getPC() {
		return pc;
	}
	public LoopType getLoopType() {
		return loopType;
	}
	public void setLoopType(LoopType loopType) {
		this.loopType = loopType;
	}
	public String getCondition() {
		return condition;
	}
	public void setCondition(String condition) {
		this.condition = condition;
	}
	public BranchAnalyzer getNextBranch() {
		return nextBranch;
	}
	public void setNextBranch(BranchAnalyzer nextBranch) {
		this.nextBranch = nextBranch;
	}
	public int getCounter() {
		return counter;
	}
	public void setCounter(int counter) {
		this.counter = counter;
	}
	public int getStartPC() {
		return startPC;
	}
	public void setStartPC(int startPC) {
		this.startPC = startPC;
	}
	public boolean isClosed() {
		return closed;
	}
	public void setClosed(boolean closed) {
		this.closed = closed;
	}
	public int getEndPC() {
		return endPC;
	}
	public void setEndPC(int endPC) {
		this.endPC = endPC;
	}
	public int getJump() {
		return jump;
	}
	public void setJump(int jump) {
		this.jump = jump;
	}
}
