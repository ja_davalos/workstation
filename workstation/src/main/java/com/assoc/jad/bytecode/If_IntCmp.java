package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.IfConditionCollector;
import com.assoc.jad.interfaces.IInstructions;

public class If_IntCmp implements IInstructions {

	private String cmp;
	private int disp = 2;
	private StringBuilder condition = new StringBuilder();
	private StringBuilder result = new StringBuilder();
	private int branchIndex = 0;

	public If_IntCmp(String cmp) {
		this.cmp = cmp;
	}

	@Override
	public String execute(Method method, int ndx, Stack<String> operandStack, BranchAnalyzer parentBranch) {
		result.setLength(0);
		condition.setLength(0);
		IfConditionCollector ifConditionCollector = new IfConditionCollector(cmp);
		ifConditionCollector.execute( method,  ndx, operandStack,  parentBranch);
		result.append(ifConditionCollector.getOutputLine());
		condition.append(ifConditionCollector.getCondition());
		disp = ifConditionCollector.getDisplacement();
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	@Override
	public String getCondition() {
		String wrkstr = condition.toString();
		condition = new StringBuilder();
		return wrkstr;
	}
	@Override
	public String getFormatString() {
		return null;
	}
	public int getBranchIndex() {
		return branchIndex;
	}
	public void setBranchIndex(int branchIndex) {
		this.branchIndex = branchIndex;
	}
}
