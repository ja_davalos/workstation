package com.assoc.jad.bytecode;

import java.util.HashMap;

import com.assoc.jad.interfaces.IInstructions;

public class InstructionClazzes {
	
	public static HashMap<Byte, IInstructions> instructions = instructionsInit();
	
	private static HashMap<Byte,IInstructions> instructionsInit() {
		
		HashMap<Byte, IInstructions> allInstructions = new HashMap<>(300);
		
		allInstructions.put((byte)0x02, new IntConst_X(-1));
		allInstructions.put((byte)0x03, new IntConst_X(0));
		allInstructions.put((byte)0x04, new IntConst_X(1));
		allInstructions.put((byte)0x05, new IntConst_X(2));
		allInstructions.put((byte)0x06, new IntConst_X(3));
		allInstructions.put((byte)0x07, new IntConst_X(4));
		allInstructions.put((byte)0x08, new IntConst_X(5));
		allInstructions.put((byte)0x10, new ByteIntPush());
		allInstructions.put((byte)0x11, new SIPush());
		allInstructions.put((byte)0x12, new Ldc());
		allInstructions.put((byte)0x15, new IntLoad());
		allInstructions.put((byte)0x18, new Dload());
		allInstructions.put((byte)0x1a, new IntLoad_X(0));
		allInstructions.put((byte)0x1b, new IntLoad_X(1));
		allInstructions.put((byte)0x1c, new IntLoad_X(2));
		allInstructions.put((byte)0x1d, new IntLoad_X(3));
		allInstructions.put((byte)0x2a, new Aload_X(0));
		allInstructions.put((byte)0x2b, new Aload_X(1));
		allInstructions.put((byte)0x2c, new Aload_X(2));
		allInstructions.put((byte)0x2d, new Aload_X(3));
		allInstructions.put((byte)0x26, new DLoad_X(0));
		allInstructions.put((byte)0x27, new DLoad_X(1));
		allInstructions.put((byte)0x28, new DLoad_X(2));
		allInstructions.put((byte)0x29, new DLoad_X(3));
		allInstructions.put((byte)0x36, new IntStore());
		allInstructions.put((byte)0x3a, new AStore());
		allInstructions.put((byte)0x3b, new IntStore_X(0));
		allInstructions.put((byte)0x3c, new IntStore_X(1));
		allInstructions.put((byte)0x3d, new IntStore_X(2));
		allInstructions.put((byte)0x3e, new IntStore_X(3));
		allInstructions.put((byte)0x59, new Dup());
		allInstructions.put((byte)0x60, new IntAdd());
		allInstructions.put((byte)0x84, new IntInc());
		allInstructions.put((byte)0xa7, new GoTo());
		allInstructions.put((byte)0x9f, new If_IntCmp("eq"));
		allInstructions.put((byte)0xa0, new If_IntCmp("ne"));
		allInstructions.put((byte)0xa1, new If_IntCmp("lt"));
		allInstructions.put((byte)0xa2, new If_IntCmp("ge"));
		allInstructions.put((byte)0xa3, new If_IntCmp("gt"));
		allInstructions.put((byte)0xa4, new If_IntCmp("le")); 
		allInstructions.put((byte)0xaa, new TableSwitch());
		allInstructions.put((byte)0xab, new LookupSwitch());
		allInstructions.put((byte)0xb0, new AReturn());
		allInstructions.put((byte)0xb1, new Return());
		allInstructions.put((byte)0xb2, new GetStatic());
		allInstructions.put((byte)0xb4, new GetField());
		allInstructions.put((byte)0xb5, new PutField());
		allInstructions.put((byte)0xb6, new InvokeVirtual());
		allInstructions.put((byte)0xb7, new InvokeSpecial());
		allInstructions.put((byte)0xb8, new InvokeStatic());
		allInstructions.put((byte)0xbb, new NewObj());
		
		allInstructions.put((byte)0x99, new If_Int("eq"));
		allInstructions.put((byte)0x9a, new If_Int("ne"));
		allInstructions.put((byte)0x9b, new If_Int("lt"));
		allInstructions.put((byte)0x9c, new If_Int("ge"));
		allInstructions.put((byte)0x9d, new If_Int("gt"));
		allInstructions.put((byte)0x9e, new If_Int("le")); 
		allInstructions.put((byte)0x19, new ALoad()); 
		allInstructions.put((byte)0x4b, new AStore_X(0)); 
		allInstructions.put((byte)0x4c, new AStore_X(1)); 
		allInstructions.put((byte)0x4d, new AStore_X(2)); 
		allInstructions.put((byte)0x4e, new AStore_X(3)); 
		allInstructions.put((byte)0xbe, new ArrayLength()); 
		allInstructions.put((byte)0xac, new IReturn()); 
		allInstructions.put((byte)0xc7, new IfNonNull()); 
		allInstructions.put((byte)0x7a, new IShR()); 
		allInstructions.put((byte)0x91, new I2B()); 
		allInstructions.put((byte)0x7e, new IAnd()); 
		allInstructions.put((byte)0x68, new IMul()); 
		allInstructions.put((byte)0x57, new Pop()); 
		allInstructions.put((byte)0xbd, new ANewArray()); 
		allInstructions.put((byte)0x53, new AAStore()); 
		allInstructions.put((byte)0xb3, new PutStatic()); 
		allInstructions.put((byte)0x6c, new IDiv()); 
		allInstructions.put((byte)0x01, new AConstNull()); 
		allInstructions.put((byte)0x87, new I2D()); 
		allInstructions.put((byte)0x14, new LDC2_W()); 
		allInstructions.put((byte)0x8e, new D2I()); 
		allInstructions.put((byte)0x5c, new Dup2()); 
		allInstructions.put((byte)0x32, new AALoad()); 
		allInstructions.put((byte)0x5a, new Dup_X1()); 		
		allInstructions.put((byte)0x5b, new Dup_X2()); 		
		allInstructions.put((byte)0x5f, new Swap()); 		
		allInstructions.put((byte)0x13, new LDC_W()); 		
		allInstructions.put((byte)0x70, new IRem()); 		
		allInstructions.put((byte)0x64, new ISub()); 		
		allInstructions.put((byte)0xc6, new IfNull()); 		
		
		return allInstructions;
	}
}
