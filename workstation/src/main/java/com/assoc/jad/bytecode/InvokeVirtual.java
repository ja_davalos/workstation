package com.assoc.jad.bytecode;

import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.bcel.classfile.ConstantClass;
import org.apache.bcel.classfile.ConstantMethodref;
import org.apache.bcel.classfile.ConstantNameAndType;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.ConstantUtf8;
import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.Method;
import org.apache.bcel.classfile.Signature;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.interfaces.IInstructions;
/**
 * Invoke instance method; dispatch based on class</br>
 * invokevirtual
 * 	indexbyte1
 * 	indexbyte2</br>
 * for more info see <a href="https://docs.oracle.com/javase/specs/jvms/se7/html/jvms-6.html#jvms-6.5.invokevirtual">invokevirtual</a>
 * @author jorge
 *
 */
public class InvokeVirtual implements IInstructions {
	private int disp = 2;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx,  Stack<String> operandStack,BranchAnalyzer parentBranch) {
		result.setLength(0);
		Utilities utils = new Utilities();
		int cpIndex =  utils.getIndexFromCode(method, ndx,disp);
		ConstantPool constantPool = method.getConstantPool();
		ConstantMethodref methodRef = (ConstantMethodref) constantPool.getConstant(cpIndex);
		int classIndex = methodRef.getClassIndex();
		ConstantClass constantClass = (ConstantClass)constantPool.getConstant(classIndex);
		ConstantUtf8 classUtf8 = (ConstantUtf8)constantPool.getConstant(constantClass.getNameIndex());
		ConstantNameAndType constantNameAndType = (ConstantNameAndType)constantPool.getConstant(methodRef.getNameAndTypeIndex());
		String methodname = constantNameAndType.getName(constantPool);
		ConstantUtf8 signature = (ConstantUtf8)constantPool.getConstant(constantNameAndType.getSignatureIndex());
		
		if (operandStack.isEmpty()) return null;
		if (classUtf8.getBytes().equals("java/lang/StringBuilder") || classUtf8.getBytes().equals("java/lang/StringBuffer")) {
			LocalVariable lv = utils.localVariableByName(method, operandStack.get(0), ndx);
			if (lv == null) {
				Field field = utils.getAField(operandStack.get(0));
				if (field == null) return null;
			} else {
				String poolType = utils.trim(classUtf8.getBytes());
				if (!poolType.equals(utils.trim(lv.getSignature()))) return null;
			}
		}

		String parms = utils.methodOperands(operandStack, signature.getBytes());
		if (operandStack.isEmpty()) {
			result.append("."+methodname).append(parms).append(";");
		} else result.append(operandStack.pop()).append("."+methodname).append(parms).append(";");
		checkNextInstruction(operandStack,ndx,method);
		return null;

/*		String parm1 = "operandStack.pop()";
		
//		if (classUtf8.getBytes().equals("java/lang/StringBuilder") || classUtf8.getBytes().equals("java/lang/StringBuffer")) {
			if (operandStack.isEmpty()) {
				result.append(parm1).append(".").append(nameAndType.getName(constantPool)).append("()");
				operandStack.push(result.toString());
				result.setLength(0);
				return null;
			}
			String parm2 = operandStack.pop();
			if (isLocalVariableString(method,utils, parm2, ndx) )
				result.append(parm2).append(parm1).append(";");
			else 
				result.append(parm2).append(".").append(nameAndType.getName(constantPool)).append("(").append(parm1).append(");");
		return null;*/
	}
	/*
	 * delay the printing of the instruction to next one. set result to zero.
	 */
	private void checkNextInstruction(Stack<String> operandStack,int pc,Method method) {
		IInstructions compareClass = new PutStatic();
		byte[] bytecodes = method.getCode().getCode();
		IInstructions nextInstructionClass = InstructionClazzes.instructions.get(bytecodes[pc+disp+1]);
		if (compareClass.getClass().getName().equals(nextInstructionClass.getClass().getName())) {
			operandStack.removeAllElements();
			operandStack.push(result.toString());
			result.setLength(0);
		} else {	//TODO this is a hack needs to figure out how/when to leave the data in stack
			compareClass = new AStore_X(2);
			if (compareClass.getClass().getName().equals(nextInstructionClass.getClass().getName())) {
				operandStack.removeAllElements();
				operandStack.push(result.toString());
				result.setLength(0);
			}
		}
	}
	private boolean isLocalVariableString(Method method,Utilities utils, String var, int pc) {
		String pattern = "(\\w+)";
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(var);
		LocalVariable lv = null;
		if (m.find()) lv = utils.localVariableByName(method, m.group(), pc);
		if (!m.find() || lv == null) return false;
		
		String signature = lv.getSignature().substring(1);
		if (signature.indexOf("java/lang/String") == -1) return false;
		return true;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}

}
