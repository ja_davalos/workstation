package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.ConstantClass;
import org.apache.bcel.classfile.ConstantFieldref;
import org.apache.bcel.classfile.ConstantNameAndType;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.ConstantUtf8;

import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.interfaces.IInstructions;

public class GetStatic implements IInstructions {
	private int disp = 2;
//	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx,  Stack<String> operandStack,BranchAnalyzer parentBranch) {
		Utilities utils = new Utilities();
		int cpIndex =  utils.getIndexFromCode(method, ndx,disp);
		
		ConstantPool constantPool = method.getConstantPool();
		ConstantFieldref fieldRef = (ConstantFieldref) constantPool.getConstant(cpIndex);
		ConstantNameAndType nameAndType = (ConstantNameAndType) constantPool.getConstant(fieldRef.getNameAndTypeIndex());
		ConstantClass clazz = (ConstantClass) constantPool.getConstant( fieldRef.getClassIndex());

		ConstantUtf8 utf8 = (ConstantUtf8) constantPool.getConstant(nameAndType.getNameIndex());
		String name = utf8.getBytes();
		utf8 = (ConstantUtf8) constantPool.getConstant(clazz.getNameIndex());
		String classname = utils.trim(utf8.getBytes())+".";
		if (classname.startsWith("this")) classname = "";
		operandStack.push(classname+name);
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return "";
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		return null;
	}

}
