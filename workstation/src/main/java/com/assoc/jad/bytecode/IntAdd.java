package com.assoc.jad.bytecode;

import java.util.Stack;


import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.interfaces.IInstructions;

public class IntAdd implements IInstructions {
	private int disp = 0;

	@Override
	public String execute(Method method, int ndx,  Stack<String> operandStack,BranchAnalyzer parentBranch) {
		String parm1 = operandStack.pop();
		String parm2 = operandStack.pop();
		operandStack.push(parm2+"+"+parm1);
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return "";
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}

}
