package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.IfConditionCollector;
import com.assoc.jad.interfaces.IInstructions;

public class IfNull implements IInstructions {
	private int disp = 2;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx, Stack<String> operandStack, BranchAnalyzer parentBranch) {
		result.setLength(0);
		
		operandStack.push("null");
		IfConditionCollector ifConditionCollector = new IfConditionCollector("eq");
		ifConditionCollector.execute( method,  ndx, operandStack,  parentBranch);
		result.append(ifConditionCollector.getOutputLine());
		disp = ifConditionCollector.getDisplacement();
		
		//result.append("if (").append(operandStack.pop()).append(" == null ) ");
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		return null;
	}

}
