package com.assoc.jad.bytecode.utils;

import java.util.Stack;

import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.bytecode.utils.BranchAnalyzer.LoopType;

public class IfConditionCollector {

	private String cmp;
	private int disp = 2;
	private StringBuilder condition = new StringBuilder();
	private StringBuilder result = new StringBuilder();
	private int branchIndex = 0;

	public IfConditionCollector(String cmp) {
		this.cmp = cmp;
	}
	public String execute(Method method, int ndx, Stack<String> operandStack, BranchAnalyzer parentBranch) {
		result.setLength(0);
		byte[] codes = method.getCode().getCode();
		branchIndex = (codes[ndx + 1] << 8) | codes[ndx + 2];
		Utilities utils = new Utilities();
		condition.append(utils.bldCondition(operandStack,cmp));
		
		BranchAnalyzer rootBranch = bldBranch(ndx,ndx+disp+1);
		if (parentBranch == null) {
			collectCondition(method,ndx,rootBranch);
			bldConditionStatement(ndx,rootBranch);
		}
		return null;
	}
	private void bldConditionStatement(int pc,BranchAnalyzer rootBranch) {		
		result.append("if ( ");
		while (rootBranch != null) {
			result.append(rootBranch.getCondition()).append(" AND ");
			disp = rootBranch.getStartPC()-pc-1;
			rootBranch = rootBranch.getNextBranch();
		}
		//remove last condition
		result.setLength(result.length()-4);
		result.append(") {");
	}
	private void collectCondition(Method method,int pc,BranchAnalyzer rootBranch) {		
		InstructionProcessor instructionProcessor = new InstructionProcessor(new Stack<String>(), method);
		int upperEnd = pc + branchIndex;
		int i=pc+disp+1;
		while (i<upperEnd) {
			instructionProcessor.process(i,null, rootBranch);
			i = instructionProcessor.getPointer();
			i++;
		}
	}
	private BranchAnalyzer bldBranch(int pc, int nextInst) {
		BranchAnalyzer nextBranch = new BranchAnalyzer(pc+branchIndex);
		if (branchIndex > 0) { // the java instruction text should be "if"......
			nextBranch.setLoopType(LoopType.IFTHEN);
			nextBranch.setCondition(condition.toString());
			nextBranch.setStartPC(nextInst);
			nextBranch.setEndPC(pc+branchIndex);
			BranchAnalyzer gotoBranch = Utilities.loopEntryToExitPoint.get(pc+branchIndex);
			if (gotoBranch == null) {
				Utilities.loopEntryToExitPoint.put(pc+branchIndex, nextBranch); // first entry
				// check next instruction it might be the last if condition
				gotoBranch = Utilities.loopEntryToExitPoint.get(nextInst);
			} 
			if (gotoBranch != null) {
				while (gotoBranch.getNextBranch() != null)
					gotoBranch = gotoBranch.getNextBranch();
				gotoBranch.setNextBranch(nextBranch);
			}
		}
		return nextBranch;
	}

	public int getDisplacement() {
		return disp;
	}
	public String getOutputLine() {
		return result.toString();
	}
	public String getCondition() {
		String wrkstr = condition.toString();
		condition = new StringBuilder();
		return wrkstr;
	}
	public String getFormatString() {
		return null;
	}
	public int getBranchIndex() {
		return branchIndex;
	}
	public void setBranchIndex(int branchIndex) {
		this.branchIndex = branchIndex;
	}
}
