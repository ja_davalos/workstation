package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.LineNumber;
import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.InstructionProcessor;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.bytecode.utils.BranchAnalyzer.LoopType;
import com.assoc.jad.interfaces.IInstructions;

public class GoTo implements IInstructions {
	private int disp = 2;
	private StringBuilder result = new StringBuilder();
	private StringBuilder formatted = new StringBuilder();

	@Override
	public String execute(Method method, int ndx,  Stack<String> operandStack,BranchAnalyzer parentBranch) {

		formatted.setLength(0);
		result.setLength(0);
		int offset = method.getCode().getCode()[ndx+1] << 8 |  method.getCode().getCode()[ndx+2];
		if (parentBranch != null) parentBranch.setEndPC(offset+ndx);
		
		if (!nextInstrHasContext(offset , ndx,disp+1) ) {
			BranchAnalyzer rootBranch = parentBranch;
			if (rootBranch == null ) {
				rootBranch = new BranchAnalyzer(ndx);
				rootBranch.setLoopType(LoopType.FOR);
			}
			fndBranchCondition(method,ndx,offset,rootBranch);
		}
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	@Override
	public String getCondition() {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public String getFormatString() {
		return formatted.toString();
	}
	private boolean nextInstrHasContext(int offset, int pc,int instrDisp) {
		BranchAnalyzer ba =  Utilities.loopEntryToExitPoint.get(pc+instrDisp);
		if (ba != null) {
			BranchAnalyzer gotoBranch = Utilities.loopEntryToExitPoint.get(pc+offset);
			BranchAnalyzer nextBranch = new BranchAnalyzer(pc+offset);
			nextBranch.setStartPC(pc);
			if (gotoBranch != null) {
				while (gotoBranch.getNextBranch() != null) gotoBranch = gotoBranch.getNextBranch();
				gotoBranch.setNextBranch(nextBranch);
			} else {
				Utilities.loopEntryToExitPoint.put(pc+offset, nextBranch); 	//first entry 
			}
			nextBranch.setLoopType(LoopType.IFTHENELSE);
			ba.setNextBranch(nextBranch);
			return true;
		}
		return false;
	}
	private void fndBranchCondition(Method method,int ndx,int offset,BranchAnalyzer rootBranch) {
		Stack<String> operandStack = new Stack<String>();
		StringBuilder javaCode = new StringBuilder();
		
		InstructionProcessor instructionProcessor = new InstructionProcessor(operandStack,method);
		for (int i=ndx+offset;i<=ndx+offset+6;i++) {
			if (!instructionProcessor.process(i,null, rootBranch)) break;
			i = instructionProcessor.getPointer();
			String condition = instructionProcessor.getCondition().toString();
			if (condition != null && condition.length() != 0) {
				BranchAnalyzer ba =  bldBranchAnalyzer(ndx,offset, i, javaCode.append(condition));
				int loopFlag = bldLoopFlag(ndx,method);
				BldLoopInstrHeader(method,ba,loopFlag);
				break;
			}
		}
		if (rootBranch != null) rootBranch.setEndPC(ndx+offset);
		
	}
	private BranchAnalyzer bldBranchAnalyzer(int ndx,int offset,int i,StringBuilder javaCode) {
		BranchAnalyzer ba =  new BranchAnalyzer(++i);
		ba.setCondition(javaCode.toString());
		ba.setStartPC(ndx);
		BranchAnalyzer gotoBranch = Utilities.loopEntryToExitPoint.get(ndx+offset);
		if (gotoBranch == null) Utilities.loopEntryToExitPoint.put(ndx+offset, ba);
		else {
			if (gotoBranch.getLoopType().name().equals(LoopType.IFTHENELSE.name())) {
				if (!gotoBranch.isClosed() && gotoBranch.getStartPC() < ndx) checkForOptimization(ndx,gotoBranch) ;
				while (gotoBranch.getNextBranch() != null) {
					if (!gotoBranch.isClosed() && gotoBranch.getStartPC() < ndx) checkForOptimization(ndx,gotoBranch) ;
					gotoBranch = gotoBranch.getNextBranch();
				}
				gotoBranch.setNextBranch(ba);
			}
		}
		return ba;
	}
	private void  BldLoopInstrHeader(Method method,BranchAnalyzer ba,int loopFlag) {
		//String name = condExpr.split(" ")[0];	//first element has the variable name.
		switch (loopFlag) {
		case 0: result.append("while (").append(ba.getCondition());ba.setLoopType(LoopType.WHILE);break;
		case -1: formatted.append("for ( %s ").append(ba.getCondition()).append(';');ba.setLoopType(LoopType.FOR);break;
		}
		result.append(" ) {");
	}
	private int bldLoopFlag(int ndx,Method method) {
		LineNumber[] lineNumArray = method.getLineNumberTable().getLineNumberTable();
		int loopFlag = -1;
		for (int i=0;i<lineNumArray.length;i++) {
			if (lineNumArray[i].getStartPC() == ndx) {
				for (int j=i;j<lineNumArray.length;j++) {
					loopFlag = 0;	//TODO
					if (lineNumArray[j].getLineNumber() == lineNumArray[i].getLineNumber())
						loopFlag = 0;	//TODO 
				}
			}
		}
		return loopFlag;
	}
	/*
	 * Optimizing has remove a goto to because it points to a goto instruction therefore they end on the same goto
	 */
	private void checkForOptimization(int ndx, BranchAnalyzer gotoBranch) {	//TODO it works only in if then else
		if (this.getClass().getName().endsWith("GoTo"))
			result.append("}").append(System.lineSeparator()); 
		gotoBranch.setClosed(true);
	}
}
