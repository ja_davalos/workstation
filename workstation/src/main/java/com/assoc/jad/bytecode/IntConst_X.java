package com.assoc.jad.bytecode;

import java.util.Stack;


import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.interfaces.IInstructions;

/**
 * Push the int constant &#60;X> (-1, 0, 1, 2, 3, 4 or 5) onto the operand stack.
 */
public class IntConst_X implements IInstructions {
	private int disp = 0;
	private int InstValue;
	
	public IntConst_X(int value) {
		this.InstValue = value;
	}
	@Override
	public String execute(Method method, int ndx, Stack<String> operandStack,BranchAnalyzer parentBranch) {
		Integer VALUE = InstValue;
		operandStack.push(VALUE.toString());
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return "";
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}

}
