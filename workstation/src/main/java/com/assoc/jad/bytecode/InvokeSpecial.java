package com.assoc.jad.bytecode;

import java.util.Stack;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.bcel.classfile.ConstantClass;
import org.apache.bcel.classfile.ConstantMethodref;
import org.apache.bcel.classfile.ConstantNameAndType;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.ConstantUtf8;

import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.interfaces.IInstructions;

/**
 * Invoke instance method; special handling for superclass, private, and instance initialization method invocations<br/>
 * The unsigned indexbyte1 and indexbyte2 are used to construct an index into the run-time constant pool of the current class (§2.6), 
 * where the value of the index is (indexbyte1 << 8) | indexbyte2. 
 * The run-time constant pool item at that index must be a symbolic reference to a method (§5.1), 
 * which gives the name and descriptor (§4.3.3) of the method as well as a symbolic reference to the class in which the method is to be found.
 * The named method is resolved (§5.4.3.3).
 * Finally, if the resolved method is protected (§4.6), and it is a member of a superclass of the current class, and the method is not declared in the same run-time package (§5.3) as the current class, then the class of objectref must be either the current class or a subclass of the current class.
 * for more info see <a href="https://docs.oracle.com/javase/specs/jvms/se7/html/jvms-6.html#jvms-6.5.invokespecial">invokespecial</a>
 */
public class InvokeSpecial implements IInstructions {
	private int disp = 2;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method,int ndx, Stack<String> operandStack,BranchAnalyzer parentBranch) {
		result.setLength(0);
		Utilities utils = new Utilities();
		int cpIndex =  utils.getIndexFromCode(method, ndx,disp);
		ConstantPool constantPool = method.getConstantPool();
		
		ConstantMethodref methodRef = (ConstantMethodref) constantPool.getConstant(cpIndex);
		ConstantClass constantClass = (ConstantClass)constantPool.getConstant(methodRef.getClassIndex()); 
		ConstantUtf8 classConstantUtf8 = (ConstantUtf8)constantPool.getConstant(constantClass.getNameIndex());
		ConstantNameAndType nameAndType = (ConstantNameAndType) constantPool.getConstant(methodRef.getNameAndTypeIndex());
		
		String classname = utils.trim(classConstantUtf8.getBytes());
		if (nameAndType.getName(constantPool).equals("<init>")) {
			if (operandStack.size() >= 2) bldfunctionParms(operandStack,classname);
			return null;
		}
		
		if (classConstantUtf8.getBytes().equals("java/lang/Object"))
			result.append(nameAndType.getName(constantPool)).append(';').append(System.lineSeparator());
		else {
			String newoper = "new "+classname+"("+operandStack.pop()+")";
			operandStack.push(newoper);
		}
		return null;
	}
	private void bldfunctionParms(Stack<String> operandStack,String classname) {
		if (classname.equals("StringBuilder")) return;

		StringBuilder parms = new StringBuilder();
		int start = matchClassname(operandStack,classname);
		if (start < 0) return;

		parms.append(operandStack.get(start++));
		int len = parms.length()-1;
		if (parms.charAt(len) == ')') parms.setLength(len);
		
		for (int i=start;i<operandStack.size();i++) {
			parms.append(operandStack.get(i)).append(',');
		}
		start--;
		while (operandStack.size() > start) operandStack.removeElementAt(start);
		len = parms.length()-1;
		if (parms.charAt(len) == ',') parms.setLength(len);
		parms.append(')');
		operandStack.push(parms.toString());

	}
	private void signalError(Stack<String> operandStack, String classname) {
		System.err.println("operandStack dump");
		for (int i=0;i<operandStack.size();i++) {
			System.err.println(operandStack.get(i));
		}
		System.err.println("classname="+classname);
		Exception e = new Exception("InvokeSpecial::");
		e.printStackTrace();
	}
	public int matchClassname(Stack<String> operandStack,String classname) {
		String pattern = "(\\w+)\\W+(\\w+)";
		Pattern r = Pattern.compile(pattern);
		for (int i=0;i<operandStack.size();i++) {
			if (operandStack.get(i).equals("this")) continue;
			Matcher m = r.matcher(operandStack.get(i));
			if (m.find()) if (m.group(2).equals(classname)) 
				return i;
		}
		return -1;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}

}
