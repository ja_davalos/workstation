package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.ConstantFieldref;
import org.apache.bcel.classfile.ConstantNameAndType;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.ConstantUtf8;
import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.interfaces.IInstructions;

public class GetField implements IInstructions {
	private int disp = 2;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx,  Stack<String> operandStack,BranchAnalyzer parentBranch) {
		
		Utilities utils = new Utilities();
		int cpIndex =  utils.getIndexFromCode(method, ndx,2);
		ConstantPool constantPool = method.getConstantPool();	
		ConstantFieldref fieldRef = (ConstantFieldref) constantPool.getConstant(cpIndex);
		cpIndex = fieldRef.getNameAndTypeIndex();
		ConstantNameAndType nameTypeRef = (ConstantNameAndType) constantPool.getConstant(cpIndex);
		ConstantUtf8 objUtf8 = (ConstantUtf8)constantPool.getConstant(nameTypeRef.getNameIndex());
		
		String refName = objUtf8.getBytes();
		String clazz = "";
		if (!operandStack.isEmpty()) clazz = operandStack.pop()+".";
		operandStack.push(clazz+refName);
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}
}
