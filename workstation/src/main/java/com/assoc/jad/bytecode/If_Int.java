package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.IfConditionCollector;
import com.assoc.jad.interfaces.IInstructions;

public class If_Int implements IInstructions {

	private int disp = 2;
	private StringBuilder condition = new StringBuilder();
	private StringBuilder result = new StringBuilder();
	private String cmp;

	public If_Int(String cmp) {
		this.cmp = cmp;
	}
	@Override
	public String execute(Method method, int ndx, Stack<String> operandStack, BranchAnalyzer parentBranch) {
		result.setLength(0);
		condition.setLength(0);

		operandStack.push("0");
		IfConditionCollector ifConditionCollector = new IfConditionCollector(cmp);
		ifConditionCollector.execute( method,  ndx, operandStack,  parentBranch);
		result.append(ifConditionCollector.getOutputLine());
		disp = ifConditionCollector.getDisplacement();
		
		if (parentBranch != null) {
			parentBranch.setJump(ifConditionCollector.getBranchIndex()+ndx);
			return null;
		}
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	@Override
	public String getCondition() {
		return condition.toString();
	}
	@Override
	public String getFormatString() {
		return null;
	}
}
