package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.interfaces.IInstructions;

public class IDiv implements IInstructions {
	StringBuilder result = new StringBuilder();
	private int disp = 0;

	@Override
	public String execute(Method method, int ndx, Stack<String> operandStack, BranchAnalyzer parentBranch) {
		String divisor  = operandStack.pop();
		String dividend = operandStack.pop();
		operandStack.push(dividend+" / "+divisor);
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		return null;
	}

}
