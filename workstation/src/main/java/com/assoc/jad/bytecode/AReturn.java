package com.assoc.jad.bytecode;

import java.util.Stack;


import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.interfaces.IInstructions;

public class AReturn implements IInstructions {
	private int disp = 0;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int i,  Stack<String> operandStack,BranchAnalyzer parentBranch) {
		result.setLength(0);
		result.append("return ").append(operandStack.pop()).append(';');
		return null;
	}
	@Override
	public int getDisplacement() {
		// TODO Auto-generated method stub
		return disp;
	}
	@Override
	public String getOutputLine() {
		// TODO Auto-generated method stub
		return result.toString();
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}

}
