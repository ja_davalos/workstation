package com.assoc.jad.bytecode;

import java.util.Stack;


import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.BranchAnalyzer.LoopType;
import com.assoc.jad.bytecode.utils.InstructionProcessor;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.interfaces.IInstructions;
/**
 * 
 * @author jorge
 *
 */
public class TableSwitch implements IInstructions {
	private int disp = 2;
	private StringBuilder result = new StringBuilder();
	private IInstructions endOnClass = new GoTo();

	@Override
	public String execute(Method method, int ndx,  Stack<String> operandStack,BranchAnalyzer parentBranch) {
		InstructionProcessor instructionProcessor = new InstructionProcessor(new Stack<String>(), method);	//TODO
		BranchAnalyzer switchBranch = new BranchAnalyzer(ndx);
		switchBranch.setLoopType(LoopType.SWITCH);
		Utilities.loopEntryToExitPoint.put(ndx, switchBranch);
		
		String switchHeader = String.format(LoopType.SWITCH.getNextCode(0),operandStack.pop()) + System.lineSeparator()+"\t";
		instructionProcessor.appendWrkJavaCode(switchHeader);
		
		byte[] codes = method.getCode().getCode();
		int ptr = ndx + 4 - ( ndx %4);
		//int defaultValue 	= (codes[ptr+0] << 24) | (codes[ptr+1] << 16) | (codes[ptr+2] << 8) | codes[ptr+3];
		int lowValue 		= (codes[ptr+4] << 24) | (codes[ptr+5] << 16) | (codes[ptr+6] << 8) | codes[ptr+7];
		int highValue 		= (codes[ptr+8] << 24) | (codes[ptr+9] << 16) | (codes[ptr+10] << 8) | codes[ptr+11];
		ptr += 12;
		int instEndPC = codes.length;
		for (int i=lowValue;i<=highValue;i++) {
			instructionProcessor.appendWrkJavaCode("case "+i+": ");
			int offset 		= (codes[ptr+0] << 24) | (codes[ptr+1] << 16) | (codes[ptr+2] << 8) | codes[ptr+3];
			boolean flag =true;
			for (int j=ndx+offset;j<instEndPC && flag;j++) {
				flag = instructionProcessor.process(j,this.endOnClass, switchBranch);
				j = instructionProcessor.getPointer();
			}
			instructionProcessor.appendWrkJavaCode(" break;");
			instructionProcessor.bldEndLine();
			instEndPC = switchBranch.getEndPC();  		//reset the endPC of the switch loop to its correct end 

			ptr += 4;
		}
		this.disp = switchBranch.getEndPC()-ndx-1;
		Utilities.javaCode.add("}"+System.lineSeparator());
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		return null;
	}

}
