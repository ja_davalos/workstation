package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.interfaces.IInstructions;

public class Swap implements IInstructions {

	@Override
	public String execute(Method method, int ndx, Stack<String> operandStack, BranchAnalyzer parentBranch) {
		String swap1 = operandStack.pop();
		String swap2 = operandStack.pop();
		operandStack.push(swap1);
		operandStack.push(swap2);
		return null;
	}

	@Override
	public int getDisplacement() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public String getOutputLine() {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public String getCondition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}

}
