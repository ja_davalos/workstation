package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.ConstantInteger;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.ConstantString;

import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.interfaces.IInstructions;

/**
 * push a constant #index from a constant pool 
 * (String, int, float, Class, java.lang.invoke.MethodType, or java.lang.invoke.MethodHandle) 
 * onto the stack
 */
public class Ldc implements IInstructions {
	private int disp = 1;

	@Override
	public String execute(Method method,int ndx, Stack<String> operandStack,BranchAnalyzer parentBranch) {
		Utilities utils = new Utilities();
		int cpIndex =  utils.getIndexFromCode(method, ndx,disp);

		ConstantPool constantPool = method.getConstantPool();
		Object obj = constantPool.getConstant(cpIndex);
		StringBuilder poolValue = new StringBuilder();
		if (obj.getClass().getName().indexOf("ConstantInteger") != -1) poolValue.append(((ConstantInteger)obj).getBytes());
		else if (obj.getClass().getName().indexOf("ConstantString") != -1) poolValue.append('"').append(((ConstantString)obj).getBytes(constantPool)).append('"');

		operandStack.push(poolValue.toString());
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		// TODO Auto-generated method stub
		return "";
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}

}
