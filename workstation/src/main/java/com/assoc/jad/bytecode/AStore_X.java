package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.interfaces.IInstructions;

public class AStore_X implements IInstructions {

	private int disp = 0;
	private int value;
	private StringBuilder result = new StringBuilder();

	public AStore_X(int value) {
		this.value = value;
	}

	@Override
	public String execute(Method method, int ndx, Stack<String> operandStack, BranchAnalyzer parentBranch) {
		result.setLength(0);
		Utilities utils = new Utilities();
		LocalVariable lv = utils.selectLocalVariable(method, value, ndx);
		result = utils.BldLocalVariableText(ndx,lv,operandStack);
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		return null;
	}
/*
 * getters and setters
 */
}
