package com.assoc.jad.bytecode;

import java.util.Stack;


import org.apache.bcel.classfile.LocalVariable;
import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.interfaces.IInstructions;

/**
 * <b>Store int into local variable</b>
 * The index is an unsigned byte that must be an index into the local variable array of the current frame (§2.6). 
 * The value on the top of the operand stack must be of type int. 
 * It is popped from the operand stack, and the value of the local variable at index is set to value.
 * @author jorge
 *
 */
public class IntStore implements IInstructions {
	private int disp = 1;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx,  Stack<String> operandStack,BranchAnalyzer parentBranch) {
		Utilities utils = new Utilities();
		int lvIndex =  utils.getIndexFromCode(method, ndx,disp);
		LocalVariable lv = method.getLocalVariableTable().getLocalVariableTable()[lvIndex];
		result = utils.BldLocalVariableText(ndx,lv,operandStack);
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}


}
