package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.interfaces.IInstructions;

public class I2B implements IInstructions {

	@Override
	public String execute(Method method, int ndx, Stack<String> operandStack, BranchAnalyzer parentBranch) {
		String toByte = operandStack.pop();
		operandStack.push("(byte)"+toByte);
		return null;
	}
	@Override
	public int getDisplacement() {
		return 0;
	}
	@Override
	public String getOutputLine() {
		return "";
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		return null;
	}

}
