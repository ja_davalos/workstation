package com.assoc.jad.bytecode.utils;

import java.util.Stack;

import org.apache.bcel.classfile.LineNumber;
import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.InstructionClazzes;
import com.assoc.jad.bytecode.utils.BranchAnalyzer.LoopType;
import com.assoc.jad.interfaces.IInstructions;

public class InstructionProcessor {
	private Stack<String> operandStack;
	private Method method;
	private StringBuilder wrkJavaCode = new StringBuilder();
	private int linePtr = -1;
	private StringBuilder indentation = new StringBuilder("\t");
	private int instPointer = 0;
	private StringBuilder condition = new StringBuilder();
	private TryCatch tryCatch = null;
	
	public InstructionProcessor(Stack<String> operandStack,Method method) {
		this.operandStack = operandStack;
		this.method = method;
		tryCatch = new TryCatch(method);
	}
	public boolean process(int ndx,IInstructions endOnClass,BranchAnalyzer parentBranch) {
		byte[] bytecodes = method.getCode().getCode();
		LineNumber[] lineNumArray = method.getLineNumberTable().getLineNumberTable();
		if (ndx >= bytecodes.length) return false;
		
		if (this.linePtr == -1) initialLinePtr(method,ndx);
		
		ndx = checkloopEntryToExitPoint(lineNumArray,ndx,linePtr);
		ndx = checkIfTryCatch(ndx);
		
		IInstructions instructionClass = InstructionClazzes.instructions.get(bytecodes[ndx]);
		if (instructionClass == null) {
			String wrkString = String.format("instruction not implemented hex=%2x dec=%2d PC=%3d", bytecodes[ndx],bytecodes[ndx],ndx);
			System.err.println(wrkString);
			System.out.println(wrkJavaCode.toString());
			this.instPointer++;
			return false;
		}
		instructionClass.execute(method,ndx,operandStack, parentBranch);
		bldInstructionOutput(instructionClass);
		checkIfTryCatch(ndx);
		
		instPointer = ndx+instructionClass.getDisplacement();
		if (endOnClass != null && endOnClass.getClass().getName().equals(instructionClass.getClass().getName())) {
			return false;
		}
		String wrkstr = instructionClass.getCondition();
		if (wrkstr != null && wrkstr.length() != 0) {
			condition.append(wrkstr);
		}
		return true;
	}
	private void bldInstructionOutput(IInstructions instructionClass) {
		bldFormatted(instructionClass);
		String output = instructionClass.getOutputLine();
		if (output.startsWith(".")) {
			int len = wrkJavaCode.length()-1;
			if (wrkJavaCode.charAt(len) == ';') wrkJavaCode.setLength(len);
		}
		wrkJavaCode.append(instructionClass.getOutputLine());
		
	}
	private int checkIfTryCatch(int ndx) {		
		tryCatch.codeException(ndx);
		int pc = tryCatch.getPc();
		if (tryCatch.isTryCatchBlock()) addJavaLine(ndx);
		wrkJavaCode.append(tryCatch.getJavaCode());
		return pc;
	}
	private int checkloopEntryToExitPoint(LineNumber[] lineNumArray,int pc,int linePtr) {
		
		BranchAnalyzer ba = Utilities.loopEntryToExitPoint.get(pc);
		if (ba != null) {
			BranchAnalyzer nextBranch = ba.getNextBranch();		
			StringBuilder sb = new StringBuilder();
			int flag = wrkJavaCode.toString().indexOf('{');
			if (flag != -1) wrkJavaCode.append("}");
			linePtr = lineBreak(lineNumArray,pc,linePtr,"");
			if (flag == -1) sb.append(this.indentation).append(wrkJavaCode.toString()).append('}');
			wrkJavaCode.setLength(0);
			
			ba.setClosed(true);
			pc = ba.getPC();
			if (nextBranch != null && ba.getLoopType().equals(LoopType.IFTHEN)) {
				if (nextBranch.getLoopType().equals(LoopType.IFTHENELSE)) 
					sb.append(nextBranch.getLoopType().getNextCode(0));//TODO
			} else {
				nextBranch = ba;
				while (nextBranch != null ) {
					if (nextBranch != null && !nextBranch.isClosed() && nextBranch.getLoopType().equals(LoopType.IFTHENELSE)) 
						sb.append(nextBranch.getLoopType().getNextCode(0));//TODO
					nextBranch = nextBranch.getNextBranch();
				}
			}
			sb.append(System.lineSeparator());
			Utilities.javaCode.add(sb.toString());
		}
		return pc;
	}
	private void initialLinePtr(Method method2,int PC) {
		LineNumber[] lineNumArray = method.getLineNumberTable().getLineNumberTable();		
		for (int i=0;i<lineNumArray.length;i++) {
			if (lineNumArray[i].getStartPC() >= PC) linePtr = i;
			if (lineNumArray[i].getStartPC() >= PC) break;
		}
	}
	private void bldFormatted(IInstructions instructionClass) {
		String wrkstr = instructionClass.getFormatString();	
		if (wrkstr == null || wrkstr.length() == 0) return;
		wrkstr = String.format(wrkstr, wrkJavaCode);
		this.wrkJavaCode.setLength(0);
		this.wrkJavaCode.append(wrkstr);
	}
	private int lineBreak(LineNumber[] lineNumArray,int PC, int linePtr,String output) {
		
		int prevLine = lineNumArray[linePtr].getLineNumber();
		for (int i=linePtr;i<lineNumArray.length;i++) {
			if (lineNumArray[i].getStartPC() == PC) linePtr = i;
			if (lineNumArray[i].getStartPC() >= PC) break;
		}
		
		int lineNum = lineNumArray[linePtr].getLineNumber();
		StringBuilder sb = new StringBuilder();
		if (prevLine != lineNum || PC == Utilities.EOCODE) {
			if (wrkJavaCode.length() > 0) {
				if (Utilities.ADDLINENUMBER) sb.append(prevLine);
				sb.append('\t').append(wrkJavaCode).append(System.lineSeparator());
				Utilities.javaCode.add(sb.toString());
				wrkJavaCode.setLength(0);
			}
		}
		if (output.length() > 0 ) wrkJavaCode.append(output);

		return linePtr;
	}
	/*
	 * getters and setters
	 */
	public StringBuilder getWrkJavaCode() {
		return wrkJavaCode;
	}
	public void setWrkJavaCode(StringBuilder wrkJavaCode) {
		this.wrkJavaCode = wrkJavaCode;
	}
	public void appendWrkJavaCode(String wrkCode) {
		this.wrkJavaCode.append(wrkCode);
	}
	public int getPointer() {
		return instPointer;
	}
	public void setPointer(int instPointer) {
		this.instPointer = instPointer;
	}
	public StringBuilder getCondition() {
		return condition;
	}
	public void setCondition(StringBuilder condition) {
		this.condition = condition;
	}
	public void bldEndLine() {
		lineBreak(method.getLineNumberTable().getLineNumberTable(),Utilities.EOCODE,linePtr,wrkJavaCode.toString());
		wrkJavaCode.setLength(0);
	}
	public void addJavaLine(int ndx) {
		linePtr  = lineBreak(method.getLineNumberTable().getLineNumberTable(),ndx,linePtr,"");
	}
	public Stack<String> getOperandStack() {
		return operandStack;
	}
	public void setOperandStack(Stack<String> operandStack) {
		this.operandStack = operandStack;
	}

}
