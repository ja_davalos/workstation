package com.assoc.jad.bytecode;

import java.util.Stack;


import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.InstructionProcessor;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.bytecode.utils.BranchAnalyzer.LoopType;
import com.assoc.jad.interfaces.IInstructions;

public class LookupSwitch implements IInstructions {
	private int disp = 2;
	private StringBuilder result = new StringBuilder();
	private IInstructions endOnClass = new Ldc();

	@Override
	public String execute(Method method, int ndx,  Stack<String> operandStack,BranchAnalyzer parentBranch) {
		InstructionProcessor instructionProcessor = new InstructionProcessor(new Stack<String>(), method);	//TODO
		BranchAnalyzer switchBranch = new BranchAnalyzer(ndx);
		switchBranch.setLoopType(LoopType.SWITCH);
		Utilities.loopEntryToExitPoint.put(ndx, switchBranch);
		
		String wrkstr = String.format(LoopType.SWITCH.getNextCode(0),operandStack.pop());
		StringBuilder switchCmd = new StringBuilder();
		switchCmd.append(wrkstr).append(System.lineSeparator()).append("\t");
		
		byte[] codes = method.getCode().getCode();
		int ptr = ndx + 4 - ( ndx %4);
//		int defaultVal 	= (codes[ptr+0] << 24) | (codes[ptr+1] << 16) | (codes[ptr+2] << 8) | codes[ptr+3];
		int npairs 		= (codes[ptr+4] << 24) | (codes[ptr+5] << 16) | (codes[ptr+6] << 8) | codes[ptr+7];
		ptr += 8;
		for (int i=0;i<npairs;i++) {
//			int match 		= (codes[ptr+0] << 24) | (codes[ptr+1] << 16) | (codes[ptr+2] << 8) | codes[ptr+3];
			int offset		= (codes[ptr+4] << 24) | (codes[ptr+5] << 16) | (codes[ptr+6] << 8) | codes[ptr+7];
			boolean flag =true;
			int j=ndx+offset;
			endOnClass = new Ldc();

			for (;j<codes.length && flag;j++) {
				flag = instructionProcessor.process(j,this.endOnClass, switchBranch);
				j = instructionProcessor.getPointer();
			}
			if (!instructionProcessor.getOperandStack().isEmpty())
				switchCmd.append("case "+instructionProcessor.getOperandStack().peek()+": ");
			
			this.endOnClass = new If_Int("ne"); flag = true;
			for (;j<codes.length && flag;j++) {
				flag = instructionProcessor.process(j,this.endOnClass, switchBranch);
				j = instructionProcessor.getPointer();
			}
			this.endOnClass = new GoTo(); flag = true;
			for (;j<codes.length && flag;j++) {
				flag = instructionProcessor.process(j,this.endOnClass, switchBranch);
				j = instructionProcessor.getPointer();
			}
			instructionProcessor.getWrkJavaCode().setLength(0);
			this.endOnClass = new GoTo(); flag = true;
			for (j=switchBranch.getJump();j<switchBranch.getEndPC() && flag;j++) {
				flag = instructionProcessor.process(j,this.endOnClass, switchBranch);
				j = instructionProcessor.getPointer();
			}
			switchCmd.append(instructionProcessor.getWrkJavaCode()).append(" break;");
			instructionProcessor.getWrkJavaCode().setLength(0);
			instructionProcessor.appendWrkJavaCode(switchCmd.toString());
			instructionProcessor.bldEndLine();
			switchCmd.setLength(0);
			instructionProcessor.getOperandStack().removeAllElements();
			ptr += 8;
		}
		this.disp = switchBranch.getEndPC()-ndx-1;
		Utilities.javaCode.add(switchCmd.toString()+System.lineSeparator());
		Utilities.javaCode.add("}");
		instructionProcessor.getWrkJavaCode().setLength(0);
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		return null;
	}

}
