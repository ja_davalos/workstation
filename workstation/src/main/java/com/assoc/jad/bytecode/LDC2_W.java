package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.ConstantDouble;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.interfaces.IInstructions;

public class LDC2_W implements IInstructions {
	private int disp = 2;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx, Stack<String> operandStack, BranchAnalyzer parentBranch) {
		result.setLength(0);

		Utilities utils = new Utilities();
		int cpIndex =  utils.getIndexFromCode(method, ndx,disp);
		ConstantPool constantPool = method.getConstantPool();
		ConstantDouble constantDouble = (ConstantDouble)constantPool.getConstant(cpIndex);
		Double DOUBLE = constantDouble.getBytes();
		operandStack.push(DOUBLE.toString());
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		// TODO Auto-generated method stub
		return result.toString();
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		return null;
	}
}
