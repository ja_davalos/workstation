package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.interfaces.IInstructions;

public class IReturn implements IInstructions {
	private int disp = 0;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method, int ndx, Stack<String> operandStack, BranchAnalyzer parentBranch) {
		result.setLength(0); 
		Integer integer = new Integer(operandStack.pop());
		result.append("return ");
		String wrkstr = (integer == 0) ? "false;" : "true;";
		result.append(wrkstr);
		return null;
	}

	@Override
	public int getDisplacement() {
		return disp;
	}

	@Override
	public String getOutputLine() {
		// TODO Auto-generated method stub
		return result.toString();
	}

	@Override
	public String getCondition() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}

}
