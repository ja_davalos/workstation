package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.ConstantFieldref;
import org.apache.bcel.classfile.ConstantNameAndType;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.Field;
import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.interfaces.IInstructions;

/*
 * load a reference onto the stack from local variable 0
 */
public class PutField implements IInstructions {
	private int disp = 2;
	private StringBuilder result = new StringBuilder();

	@Override
	public String execute(Method method,int ndx, Stack<String> operandStack,BranchAnalyzer parentBranch) {
		result.setLength(0);
		Utilities utils = new Utilities();
		int cpIndex =  utils.getIndexFromCode(method, ndx,disp);
		
		ConstantPool constantPool = method.getConstantPool();
		ConstantFieldref fieldRef = (ConstantFieldref) constantPool.getConstant(cpIndex);
		String classname = fieldRef.getClass(constantPool);
		ConstantNameAndType nameAndType = (ConstantNameAndType) constantPool.getConstant(fieldRef.getNameAndTypeIndex());
		
		String prevValue = operandStack.pop();
		String varname = "";
		if (!operandStack.isEmpty()) varname = operandStack.pop()+".";
		Field[] fields = Utilities.javaClass.getFields();
		String cpFldName = nameAndType.getName(constantPool);
		result.append(varname+cpFldName).append(" = ").append(prevValue).append(';');

/*		varname = varname+"."+cpFldName;
		if (prevValue.indexOf(varname) != -1) {
			prevValue = prevValue.replaceFirst(varname, "").trim();
			prevValue = prevValue.substring(0,1) + "= "+prevValue.substring(1);
		}
		for (int i=0;i<fields.length;i++) {
			if (!fields[i].getName().equals(cpFldName)) continue;
			result = new StringBuilder(fields[i].toString()).append(" = ").append(prevValue).append(';');
		}*/
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return result.toString();
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}

}
