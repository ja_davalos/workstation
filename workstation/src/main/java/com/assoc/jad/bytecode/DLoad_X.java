package com.assoc.jad.bytecode;

import java.util.Stack;


import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.interfaces.IInstructions;

/*
 * load a double from local variable pointed by value.0-3
 */
public class DLoad_X implements IInstructions {
	private int disp = 0;
	@SuppressWarnings("unused")
	private int value;
	
	public DLoad_X(int value) {
		this.value = value;
	}
	@Override
	public String execute(Method method,int ndx, Stack<String> operandStack,BranchAnalyzer parentBranch) {
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return "";
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}

}
