package com.assoc.jad.bytecode;

import java.util.Stack;


import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.interfaces.IInstructions;

/**
 * <b>Push short</b>
 * The immediate unsigned byte1 and byte2 values are assembled into an intermediate short 
 * where the value of the short is (byte1 << 8) | byte2. The intermediate value is then sign-extended to an int value.
 * That value is pushed onto the operand stack.
 * @author jorge
 *
 */
public class SIPush implements IInstructions {
	private int disp = 2;

	@Override
	public String execute(Method method, int ndx,  Stack<String> operandStack,BranchAnalyzer parentBranch) {
		Utilities utils = new Utilities();
		int cpIndex =  utils.getIndexFromCode(method, ndx,disp);
		Integer WRKINT = cpIndex;
		operandStack.push(WRKINT.toString());
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return "";
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}

}
