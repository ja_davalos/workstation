package com.assoc.jad.bytecode;

import java.util.Stack;

import org.apache.bcel.classfile.ConstantClass;
import org.apache.bcel.classfile.ConstantPool;
import org.apache.bcel.classfile.ConstantUtf8;

import org.apache.bcel.classfile.Method;

import com.assoc.jad.bytecode.utils.BranchAnalyzer;
import com.assoc.jad.bytecode.utils.Utilities;
import com.assoc.jad.interfaces.IInstructions;
/**
 * <b>Create new object</b>
 * The unsigned indexbyte1 and indexbyte2 are used to construct an index into the run-time constant pool of the current class (§2.6), 
 * where the value of the index is (indexbyte1 << 8) | indexbyte2. The run-time constant pool item at the index must be a symbolic reference 
 * to a class or interface type. The named class or interface type is resolved (§5.4.3.1) and should result in a class type. 
 * Memory for a new instance of that class is allocated from the garbage-collected heap, 
 * and the instance variables of the new object are initialized to their default initial values (§2.3, §2.4). 
 * The objectref, a reference to the instance, is pushed onto the operand stack.
 * @author jorge
 */
public class NewObj implements IInstructions {
	private int disp = 2;

	@Override
	public String execute(Method method, int ndx,  Stack<String> operandStack,BranchAnalyzer parentBranch) {
		Utilities utils = new Utilities();
		int cpIndex = utils.getIndexFromCode(method, ndx,disp);
		ConstantPool constantPool = method.getConstantPool();
		Object obj = constantPool.getConstant(cpIndex);
		if (obj.getClass().getName().indexOf("ConstantClass") == -1) return null;
		ConstantClass constantClass = (ConstantClass) obj;
		String name = constantClass.getBytes(constantPool);
		operandStack.push("new "+utils.trim(name)+"()");
		return null;
	}
	@Override
	public int getDisplacement() {
		return disp;
	}
	@Override
	public String getOutputLine() {
		return "";
	}
	@Override
	public String getCondition() {
		return null;
	}
	@Override
	public String getFormatString() {
		// TODO Auto-generated method stub
		return null;
	}
}
