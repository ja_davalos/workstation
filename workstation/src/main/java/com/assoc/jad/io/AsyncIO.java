package com.assoc.jad.io;

import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;

public class AsyncIO implements Runnable {
	private LinkedList<ArrayObject> lnkedlst = new LinkedList<ArrayObject>();
	private boolean endRun = false;
	private String filename = "";
	private java.io.DataOutputStream dos = null;

	private synchronized void waitForEOT() {
		try {
			wait();
		} catch (Exception err1) {
			System.out.println("AsyncIO::waitForEOT " + err1.toString());
			err1.printStackTrace();
			System.exit(-1);
		}
	}
	private synchronized void bldfile(int len) {
		try {
			int size = lnkedlst.size();
			dos.writeInt(len);
			for (int i = 0; i < size; i++) {
				dos.write(lnkedlst.get(i).newarray, 0, lnkedlst.get(i).len);
			}
			for (int i = 0; i < size; i++) {
				lnkedlst.removeFirst();
			}
			dos.flush();
		} catch (Exception err1) {
			System.out.println("AsyncIO::bldfile " + err1.toString());
			err1.printStackTrace();
			System.exit(-1);
		}
	}
//	(new DataInputStream((new ByteArrayInputStream(bytes,0,4)))).readInt();//jadtest

	public synchronized void copyArray(byte[] srcarray,int len) {
		byte[] newarray = new byte[srcarray.length];
		System.arraycopy(srcarray, 0, newarray, 0, len);
		ArrayObject aObject = new ArrayObject(srcarray,len);
		lnkedlst.add(lnkedlst.size(),aObject);
	}
	public void run() {
		try {
			FileOutputStream outf = new FileOutputStream(filename, false);
			dos = new DataOutputStream(outf);
			while (!endRun) {
				waitForEOT();
			}
			outf.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void setEndRun() {
		endRun = true;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public void clntEndOfBlock(int len) {
		bldfile(len);
	}
}
