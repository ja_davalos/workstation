package com.assoc.jad.io;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileSplit implements Runnable {
	private String inFilename = "";
	FileOutputStream outf = null;

	public void splitFile(String inFilename) {
		String segment = "_seg";
		int len;
		byte[] bytes = new byte[16284];
		int MEG = 1024 * 1024;
		int maxbytes = 100 * MEG;

		File inputFile = new File(inFilename);
		long segmentCtr = inputFile.length() / maxbytes;
		try {
			FileInputStream inpf = new FileInputStream(inputFile);
			for (int i = 0; i <= segmentCtr; i++) {
				int ctr = maxbytes;
				File outputFile = new File(inFilename + segment + i);
				FileOutputStream outf = new FileOutputStream(outputFile);
				while ((len = inpf.read(bytes, 0, bytes.length)) != -1) {
					outf.write(bytes, 0, len);
					ctr -= len;
					if (ctr <= 0) break;
				}
				outf.close();
				if (len == -1) break;
			}
			inpf.close();
		} catch (IOException e) {
			System.out.println("FileSplit::splitFile " + e.toString());
		}
	}

	public void run() {
		splitFile(inFilename);
	}

	public void setFilename(String inFilename) {
		this.inFilename = inFilename;
	}
}
