package com.assoc.jad.io;

public class ArrayObject {
	byte[] newarray = null;
	public int len = 0;

	public ArrayObject(byte[] srcarray, int len) {
		newarray = new byte[srcarray.length];
		this.len = len;
		System.arraycopy(srcarray, 0, newarray, 0, len);
	}

}
