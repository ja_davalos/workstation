package com.assoc.jad.ui;

import java.lang.reflect.Method;

public class WorkStationAsyncTask  implements Runnable { 
	public String tname="";
	public String cmd="";
	public Object parm;
	
	private void DynamicMethod(String methnam,Object parm1) {
		if (methnam.length() ==0) return;
		
		Object[] arguments = new Object[] {parm1};
		Class<? extends WorkStationAsyncTask> c = this.getClass();
		Method method = null;
		Method[] methods = c.getMethods();
		for (int i=0;i<methods.length;i++) {
			if (methods[i].getName().equals(methnam)) {
				method = methods[i];
				i = methods.length;
			} 
		} 
		if (method == null) {
			System.out.println("WorkStationDriverAsyncTask::DynamicMethod: method not found "+methnam);
			return;
		}
		try {
			method.invoke(this,arguments);
		} catch (Exception e) {
			System.out.println("WorkStationDriverAsyncTask::DynamicMethod:"+methnam+" "+e);
		} 
	}
    public void EDServer_bin(String tag) {
    }
	
	public void run() {
		DynamicMethod(cmd,parm);
	}
}
