package com.assoc.jad.ui;

public interface MenuCommandsInterface {
	void Down();
	void Top();
	void Bottom();
	void Up();
}
