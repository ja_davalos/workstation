package com.assoc.jad.ui;

import java.awt.Dimension;
import java.io.File;
import java.io.FileOutputStream;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.assoc.jad.WorkStationDriver;

public class CRDTabPanelUpdates  implements Runnable { 
	public String fname="";
	public JTextArea PTLog;
	public JScrollPane scrollPane;
	
	private int origLen = 0;
	private boolean forever = true;
	private Dimension dim;
	
	public CRDTabPanelUpdates() {	
	}
	private void appendToLog() {
		int len = PTLog.getText().length();
		if (len <= origLen) return;

		String wrkstr = PTLog.getText().replaceAll("\r","").replaceAll("\n","\r\n");
		origLen = len;
		File outputFile = new File(fname);
		try {
			FileOutputStream fos = new FileOutputStream(outputFile);
			fos.write(wrkstr.getBytes());
			fos.close();
		} catch (Exception e) {
			System.out.println("CRDTools::appendToLog" + e.toString());
		}
	}
    private void checksize() {
		Dimension tmpDim = null;
		tmpDim = WorkStationDriver.parentFrame.getSize();
		tmpDim.width  -= 10;
		tmpDim.height -= 85;
		if (dim == null || (tmpDim.width != dim.width || tmpDim.height != dim.height)) {
			dim = tmpDim;
			scrollPane.setPreferredSize(dim);
		}
//		FontMetrics fm = getFontMetrics(WorkStationDriver.oldFont);
    }
	synchronized private void delayit() {
		try {
			while (forever) {
				this.wait(10000);
				checksize();
				appendToLog();
			}
			} catch (Exception e) {
				System.out.println("CRDTabPanelUpdates::run has ended "+e);
				e.printStackTrace();
			}
		
	}

	public void run() {
		origLen = PTLog.getText().length();
		delayit();
	}
}
