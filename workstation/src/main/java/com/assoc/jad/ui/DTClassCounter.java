package com.assoc.jad.ui;

public class DTClassCounter implements Comparable<DTClassCounter> {
	int counter=1;
	String line = "";
	String key = "";
	Integer COUNTER;
	public Integer getCOUNTER() {
		return new Integer(counter);
	}

	DTClassCounter(String key,String line) {
		this.line = line ;
		this.key = key;
	}
	
	public String getLine() {
		return line;
	}

	public void setLine(String line) {
		this.line = line;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public int getCounter() {
		return counter;
	}

	public void incCounter() {
		this.counter++;
	}

//	@Override
	public int compareTo(DTClassCounter o) {
		return getCOUNTER().compareTo(o.getCOUNTER());
	}
}
