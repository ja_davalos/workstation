package com.assoc.jad.ui;

import javax.swing.JOptionPane;

import com.assoc.jad.WorkStationDriver;

import java.util.Map;
import java.util.HashMap;
import java.util.Iterator;

public class CRDprevCopyCssJs  { 
	private String CssJsFrom;
	private String CssJsTo;
	private String CssJsName;
	private Map<String, CRDprevCopyCssJs> elements = new HashMap<String, CRDprevCopyCssJs>();
	
	public CRDprevCopyCssJs() {	
	}
	public void newCopyCssJs(String from, String to,String name) {
		if (from == null || to == null || name == null ) return;
		
		CRDprevCopyCssJs copy = new CRDprevCopyCssJs();
		copy.CssJsFrom = from;
		copy.CssJsTo   = to;
		copy.CssJsName = name;
		String key = name+from;
		elements.put(key,copy);
	}
	public CRDprevCopyCssJs selectPrevCopy() {
		if (elements.size() <= 0) return null;
		
        Iterator<String> iter = elements.keySet().iterator();
        String[] prevCopies = new String[elements.size()];
        String key = "";
        int ndx = 0;
        while (iter.hasNext()) {
        	key = (String)iter.next();
        	CRDprevCopyCssJs copy  = (CRDprevCopyCssJs)elements.get(key);
        	prevCopies[ndx++] = copy.CssJsName+" "+copy.CssJsFrom+" "+copy.CssJsTo;
        }
		String entry = (String) JOptionPane.showInputDialog(WorkStationDriver.parentFrame,
				"select command ", "select from ", JOptionPane.PLAIN_MESSAGE, null,
				prevCopies, null);
		if ( entry == null) return null;
	    String[] selected = entry.split(" ");
		return (CRDprevCopyCssJs)elements.get(selected[0]+selected[1]);
	}
	public String getCssJsFrom() {
		return CssJsFrom;
	}
	public String getCssJsTo() {
		return CssJsTo;
	}
	public String getCssJsName() {
		return CssJsName;
	}
}
