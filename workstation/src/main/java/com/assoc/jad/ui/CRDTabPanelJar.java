package com.assoc.jad.ui;

import java.awt.Dimension;
import java.io.BufferedInputStream;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.assoc.jad.WorkStationDriver;

public class CRDTabPanelJar  implements Runnable { 
	public JScrollPane scrollPane;
	
	private JTextArea Logger;
	private boolean forever = true;
	private Dimension dim;
	
	public CRDTabPanelJar(JTextArea Logger) {	
		this.Logger = Logger;
		Logger.append("\n");
	}
    private void checksize() {
		Dimension tmpDim = null;
		tmpDim = WorkStationDriver.parentFrame.getSize();
		tmpDim.width  -= 10;
		tmpDim.height -= 85;
		if (dim == null || (tmpDim.width != dim.width || tmpDim.height != dim.height)) {
			dim = tmpDim;
			scrollPane.setPreferredSize(dim);
		}
    }
	private void getJarMethods(String jar,String entry,File classfile) {
		try {
			entry = entry.replaceAll("/",".").replaceAll(".class","");
			CRDClassLoader cLoader = new CRDClassLoader(Logger);
			cLoader.classfile = classfile;
			cLoader.entry     = entry;
			cLoader.jarlocation = jar;
			new Thread(cLoader, entry).start();
		} catch (Exception e) {
			Logger.append("CRDMenuTasks::getJarMethods " + e.toString());
			Logger.setCaretPosition(Logger.getText().length());
			Logger.requestFocus();
		}
	}
	private void Display_JarEntry() {
		byte[] bytes = new byte[1024];
		String file = null;
 		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("select JAR file to extract");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = chooser.showOpenDialog(WorkStationDriver.parentFrame);
		if (returnVal == JFileChooser.CANCEL_OPTION) return;

		file = chooser.getSelectedFile().getAbsolutePath();
		try {
			JarFile jarfile = new JarFile(file);
			Enumeration<JarEntry> jarentries = jarfile.entries();
			String entries = "";
			for (; jarentries.hasMoreElements() ;) {
				entries += jarentries.nextElement().toString()+" ";
			}
			jarfile.close();
			String[] wrkDirs = entries.split(" ");
			String search = JOptionPane.showInputDialog(WorkStationDriver.parentFrame,"string to search");
			if (search != null && search.length() > 0) {
				search = search.toLowerCase();
				entries = "";
				for (int i =0;i<wrkDirs.length;i++) {
					if (wrkDirs[i].toLowerCase().indexOf(search) != -1) entries += wrkDirs[i]+" ";
				}
				
			}
			String[] buildDirs = entries.split(" ");
			String entry = (String) JOptionPane.showInputDialog(WorkStationDriver.parentFrame,
					"select jar file from directory:"+file, "select from "
							+ file + " to edit", JOptionPane.PLAIN_MESSAGE, null,
					buildDirs, search);
			if ( entry == null) return;

			File outputFile = null;
			int ndx = entry.lastIndexOf("/");
			if (ndx != -1) {
				String wrkstr = entry.substring(0,ndx);
				outputFile = new File(WorkStationDriver.local_dir+ File.separator + wrkstr);
				outputFile.mkdirs();
			}
			outputFile = new File(WorkStationDriver.local_dir+ File.separator + entry);
			FileWriter out = new FileWriter(outputFile);
			BufferedWriter outd = new BufferedWriter(out);

			int len = 0;
			BufferedInputStream src_jar = new BufferedInputStream(jarfile.getInputStream(jarfile.getEntry(entry)));
			while ((len = src_jar.read(bytes,0,bytes.length)) != -1) {
				outd.write(new String(bytes,0,len));
			}
			src_jar.close();
			outd.close();
			if (entry.indexOf(".class") != -1) {
				getJarMethods(file,entry,outputFile);
				return;
			}
		} catch (Exception e1) {
			Logger.append("CRDMenuTasks::Display_JarEntry:"+e1.toString());
			e1.printStackTrace();
			return;
		}
	}

	public void retry() {
		Display_JarEntry();
	}
	synchronized private void delayit() {
		try {
			Display_JarEntry();
			while (forever) {
				checksize();
				this.wait(10000);
			}
		} catch (Exception e) {
			System.out.println("CRDTabPanelJar::run has ended "+e);
			e.printStackTrace();
		}
		
	}
	public void run() {
		delayit();
	}
}
