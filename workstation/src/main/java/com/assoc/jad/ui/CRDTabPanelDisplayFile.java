package com.assoc.jad.ui;

import java.awt.Dimension;

import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import com.assoc.jad.WorkStationDriver;

public class CRDTabPanelDisplayFile  implements Runnable {
	public JScrollPane scrollPane;
	public boolean endOfRun = false;
	public String selectedText = null;

	private JTextArea Logger;
	private Dimension dim;
	private CRDTools crdtools = new CRDTools();

	public CRDTabPanelDisplayFile(JTextArea Logger) {
		this.Logger = Logger;
		Logger.append("\n");
	}
    private void checksize() {
		Dimension tmpDim = null;
		tmpDim = WorkStationDriver.parentFrame.getSize();
		tmpDim.width  -= 10;
		tmpDim.height -= 85;
		if (dim == null || (tmpDim.width != dim.width || tmpDim.height != dim.height)) {
			dim = tmpDim;
			scrollPane.setPreferredSize(dim);
		}
    }
	private void displayFile() {
		try {
			Logger.append(crdtools.showFile(selectedText));
		} catch (Exception e1) {
			Logger.append("CRDTabPanelDisplayFile::Display_JarEntry:"+e1.toString());
			e1.printStackTrace();
			return;
		}
	}

	public void retry() {
		Logger.setText("");
		displayFile();
	}
	synchronized private void delayit() {
		try {
			displayFile();
			while (!endOfRun) {
				this.wait(10000);
				checksize();
			}
		} catch (Exception e) {
			System.out.println("CRDTabPanelClassHex::run has ended "+e);
			e.printStackTrace();
		}

	}
	public void run() {
//		if (WorkStationDriver.classFileName == null || WorkStationDriver.classFileName.length() == 0)
//			crdtools.getClassFileName();
		delayit();
	}
}
