package com.assoc.jad.ui;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.JarURLConnection;
import java.net.URL;
import java.util.Enumeration;
import java.util.jar.JarFile;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import com.assoc.jad.WorkStationDriver;

public class CRDTools {
	static final long serialVersionUID = 7;
	private String fileName;

	public String getTaskPid(String outd) {
		String wrkstr = "";

		String[] lines = outd.split("\n");
		for (int ii=0;ii<lines.length;ii++) {
			//if (lines[ii].indexOf(WorkStationDriver.OSNameMT) == -1 ) continue;
			wrkstr = lines[ii];
		}
		if (wrkstr.length() == 0) return "";
		int ndx1 = wrkstr.indexOf(" ");
		wrkstr = wrkstr.substring(ndx1).trim();
		ndx1 = wrkstr.indexOf(" ");
		String pid = wrkstr.substring(0,ndx1).trim();
		return pid;
	}
	public String getPid(String outd) {
		int ndx1 = outd.lastIndexOf("\n");
		if (ndx1 == outd.length()-1) {
			outd = outd.substring(0,ndx1);
			ndx1 = outd.lastIndexOf("\n");
		}
		int ndx2 = outd.indexOf("pid",ndx1);
		if (ndx2 == -1) return outd.substring(ndx1);
		ndx2 = outd.indexOf("=",ndx2);
		String wrkstr = outd.substring(ndx2+1).trim();
		ndx1 = wrkstr.indexOf(" ");
		String pid = wrkstr.substring(0,ndx1);
		return pid;
	}
	public String[] readErrorTags(String file) {
		String[] command=null;
		StringBuffer sb = new StringBuffer();
		File inputFile = new File(file);
		String wrkstr;
		try {
			FileReader in = new FileReader(inputFile);
			BufferedReader    ind = new BufferedReader(in);
			while ((wrkstr = ind.readLine()) != null) {
				wrkstr.trim();
//				if (wrkstr.startsWith("#")) continue;
				if (wrkstr.length() <= 0) continue;
				if (wrkstr.startsWith("["))
					sb.append(wrkstr).append("\n");
			}
			command = sb.toString().split("\n");
			ind.close();
		} catch (IOException e) {
			command = ("CRDTools::readConfFile \n" + e.toString()).split("\n");
			return command;
		}
		return command;
	}
	public String readCategory(String file,String tag) {
		StringBuffer sb = new StringBuffer();
		File inputFile = new File(file);
		String wrkstr;
		boolean tagFound = false;
		try {
			FileReader in = new FileReader(inputFile);
			BufferedReader    ind = new BufferedReader(in);
			while ((wrkstr = ind.readLine()) != null) {
				wrkstr.trim();
				if (!tagFound && !wrkstr.equals(tag)) continue;
				if (tagFound && wrkstr.startsWith("[")) break;
				tagFound = true;
				sb.append(wrkstr).append("\n");
			}
			ind.close();
		} catch (IOException e) {
			return ("CRDTools::readConfFile \n" + e.toString());
		}
		return sb.toString();
	}
	public String readText(String file,String search) {
		if (search == null || search.trim().length() <= 0) return "";
		StringBuffer sb = new StringBuffer();
		File inputFile = new File(file);
		String wrkstr="\n";
		try {
			FileReader in = new FileReader(inputFile);
			BufferedReader    ind = new BufferedReader(in);
			while ((wrkstr = ind.readLine()) != null) {
				wrkstr.trim();
				if (!wrkstr.startsWith("[") && wrkstr.indexOf(search) == -1 ) continue;
				sb.append(wrkstr).append("\n");
			}
			ind.close();
		} catch (IOException e) {
			return ("CRDTools::readConfFile \n" + e.toString());
		}
		return sb.toString();
	}
	public String readFile(String file) {
		byte[] bytes;
		File inputFile = new File(file);
		FileInputStream inpf = null;
		try {
			int ndx1 = 0;
			int len = (int)inputFile.length();
			bytes = new byte[len];
			inpf = new FileInputStream(inputFile);
			while ((len = inpf.read(bytes,ndx1,bytes.length)) != -1) {
				if (len >= bytes.length-1) break;
				ndx1 += len;
			}
		} catch (IOException e) {
			return ("");
		} finally {
			if (inpf != null ) {
				try { inpf.close(); } catch (Exception e) {};
			}
		}
		if (bytes.length == 0) return "";
		return new String(bytes);
	}
	public byte[] readFile(File file) {
		byte[] bytes;
		try {
			int ndx1 = 0;
			int len = (int)file.length();
			bytes = new byte[len];
			FileInputStream inpf = new FileInputStream(file);
			ndx1 = inpf.read(bytes);
			inpf.close();
			if (ndx1 == -1 || len != ndx1) return null;
		} catch (IOException e) {
			return null;
		}
		return bytes;
	}
	public String showFile(String file) {
		StringBuffer sb = new StringBuffer();
		File inputFile = new File(file);
		String wrkstr="";
		try {
			FileReader in = new FileReader(inputFile);
			BufferedReader    ind = new BufferedReader(in);
			while ((wrkstr = ind.readLine()) != null) {
				sb.append(wrkstr).append("\n");
			}
			ind.close();
		} catch (IOException e) {
			return ("CRDTools::readConfFile \n" + e.toString());
		}
		return sb.toString();
	}
	public void appendToLog(String wrkfile,String text) {
		File outputFile = new File(wrkfile);
		try {
			FileOutputStream fos = new FileOutputStream(outputFile,true);
			fos.write(text.getBytes());
			fos.close();
		} catch (Exception e) {
			System.out.println("CRDTools::appendToLog" + e.toString());
		}
	}
	public String showBldTargetName(String file) {
		StringBuffer sb = new StringBuffer();
		File inputFile = new File(file);
		String wrkstr="";
		try {
			FileReader in = new FileReader(inputFile);
			BufferedReader    ind = new BufferedReader(in);
			while ((wrkstr = ind.readLine()) != null) {
				wrkstr = wrkstr.trim();
				if (wrkstr.startsWith("<target")) {
					int ndx1 = wrkstr.indexOf("name=");
					if (ndx1 == -1) continue;
					ndx1 += 5;
					int ndx2 = wrkstr.indexOf(wrkstr.charAt(ndx1),ndx1+1);
					if (ndx2 == -1) ndx2 = wrkstr.indexOf(" ",ndx1+1);
					if (ndx2 == -1) ndx2 = wrkstr.indexOf(">",ndx1+1);
					if (ndx2 == -1) ndx2 = wrkstr.length();
					wrkstr = wrkstr.substring(ndx1+1,ndx2).replaceAll(":", "");
					sb.append(wrkstr).append("\n");
				}
			}
			ind.close();
		} catch (IOException e) {
			return ("CRDTools::readConfFile \n" + e.toString());
		}
		return sb.toString();
	}
	public byte[] getClassFromJAR(String infile,String jarURL) {
		JarURLConnection uc = null;
		try {
			URL u = new URL(jarURL);
			uc = (JarURLConnection)u.openConnection();
			return getClassFromJAR(uc.getJarFile(),infile);
		} catch (IOException e) {
			System.err.println("CRDTools::getFileFromJAR:"+e.toString());
			return null;
		}
	}
	public byte[] getClassFromJAR(JarFile jarfile,String classEntry) {
		BufferedInputStream src_jar = null;
		byte[] jarbyte2 = null;
		int inbytes;
		try {
			src_jar = new BufferedInputStream(jarfile.getInputStream(jarfile.getEntry(classEntry)));
			byte[] bytes = new byte[1024];
			int len = 0;
			while ((inbytes = src_jar.read(bytes,0,bytes.length)) != -1) {
				len += inbytes;
			}
			src_jar = new BufferedInputStream(jarfile.getInputStream(jarfile.getEntry(classEntry)));
			jarbyte2 = new byte[len];
			inbytes=src_jar.read(jarbyte2);
			src_jar.close();
			if (inbytes == -1) return null;
		} catch (IOException e) {
			System.err.println("CRDTools::getFileFromJAR:"+e.toString());
		}
		return jarbyte2;
	}
	/**
	 * it dir is a jar file it will select only from that jar. if it is a directory it will select all jars
	 * from that directory
	 * display all entries from jar if search length is zero
	 * @param dir
	 * @param search
	 * @return String[]
	 */
	public String[] getClassNamesFromJARs(String dir,String search) {
		String entries = "";
		String fullname = "";
		JarFile jarfile = null;
		String[] files = null;
		try {
			File jardir = new File(dir);
			if (!jardir.isDirectory()) {
				files = new String[1];
				files[0] = jardir.getName();
				dir = jardir.getParent();
			} else files = jardir.list();

			search = search.toLowerCase();
			for (int i = 0;i<files.length;i++) {
				if (files[i].indexOf(".jar") == -1) continue;
				fullname = dir+File.separator+files[i];
				jarfile = new JarFile(fullname);
				Enumeration<?> jarentries = jarfile.entries();
				for (; jarentries.hasMoreElements() ;) {
					String entry = jarentries.nextElement().toString();
					if (entry.length() > 0 && entry.toLowerCase().indexOf(search) != -1)
						entries += fullname+";"+entry+"|";
				}
			}
		} catch (IOException e) {
			System.err.println("CRDTools::getClassNamesFromJARs:"+e.toString());
		}
		finally {
			if (jarfile != null)
				try {
					jarfile.close();
				} catch (IOException e) {}
		}
		if (entries.length() == 0) return null;
		return entries.split("\\|");
	}
	public String getClassFileName() {
 		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("select JAR file to extract");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = chooser.showOpenDialog(WorkStationDriver.parentFrame);
		if (returnVal == JFileChooser.CANCEL_OPTION) return null;

		fileName = chooser.getSelectedFile().getAbsolutePath();
		WorkStationDriver.jarFileName = fileName;
		if (chooser.getSelectedFile().getName().indexOf(".jar") != -1)
			WorkStationDriver.classFileName = fileName;
		else {
			WorkStationDriver.classFileName = fileName;
			WorkStationDriver.jarFileName = null;
		}
		return fileName;
	}
	public byte[] WriteClassFromJar(String jarname, String entry) {
		byte[] bytes = new byte[1024];

		try {
		JarFile jarfile = new JarFile(jarname);
		File outputFile = null;
		int ndx = entry.lastIndexOf("/");
		if (ndx != -1) {
			String wrkstr = entry.substring(0,ndx);
			outputFile = new File(WorkStationDriver.local_dir+ File.separator + wrkstr);
			outputFile.mkdirs();
		}
		outputFile = new File(WorkStationDriver.local_dir+ File.separator + entry);
		FileWriter out = new FileWriter(outputFile);
		BufferedWriter outd = new BufferedWriter(out);

		int len = 0;
		BufferedInputStream src_jar = new BufferedInputStream(jarfile.getInputStream(jarfile.getEntry(entry)));
		while ((len = src_jar.read(bytes,0,bytes.length)) != -1) {
			outd.write(new String(bytes,0,len));
		}
		src_jar.close();
		outd.close();
		jarfile.close();
		return readFile(outputFile);
		} catch (Exception e1) {
			WorkStationDriver.xmlLog.append("CRDTools::WriteClassFromJar:"+e1.toString());
			e1.printStackTrace();
			return null;
		}
	}
  	public void DisplayJarEntries() {
		byte[] bytes = new byte[1024];
		String file = null;
 		JFileChooser chooser = new JFileChooser();
		chooser.setDialogTitle("select JAR file to extract");
		chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		int returnVal = chooser.showOpenDialog(WorkStationDriver.parentFrame);
		if (returnVal == JFileChooser.CANCEL_OPTION) return;

		file = chooser.getSelectedFile().getAbsolutePath();
		try {
			JarFile jarfile = new JarFile(file);
			Enumeration<?> jarentries = jarfile.entries();
			String entries = "";
			for (; jarentries.hasMoreElements() ;) {
				entries += jarentries.nextElement().toString()+" ";
			}
			jarfile.close();
			String[] wrkDirs = entries.split(" ");
			String search = JOptionPane.showInputDialog(WorkStationDriver.parentFrame,"string to search");
			if (search != null && search.length() > 0) {
				search = search.toLowerCase();
				entries = "";
				for (int i =0;i<wrkDirs.length;i++) {
					if (wrkDirs[i].toLowerCase().indexOf(search) != -1) entries += wrkDirs[i]+" ";
				}

			}
			String[] buildDirs = entries.split(" ");
			String entry = (String) JOptionPane.showInputDialog(WorkStationDriver.parentFrame,
					"select jar file from directory:"+file, "select from "
							+ file + " to edit", JOptionPane.PLAIN_MESSAGE, null,
					buildDirs, search);
			if ( entry == null) return;

			File outputFile = null;
			int ndx = entry.lastIndexOf("/");
			if (ndx != -1) {
				String wrkstr = entry.substring(0,ndx);
				outputFile = new File(WorkStationDriver.local_dir+ File.separator + wrkstr);
				outputFile.mkdirs();
			}
			outputFile = new File(WorkStationDriver.local_dir+ File.separator + entry);
			FileWriter out = new FileWriter(outputFile);
			BufferedWriter outd = new BufferedWriter(out);

			int len = 0;
			BufferedInputStream src_jar = new BufferedInputStream(jarfile.getInputStream(jarfile.getEntry(entry)));
			while ((len = src_jar.read(bytes,0,bytes.length)) != -1) {
				outd.write(new String(bytes,0,len));
			}
			src_jar.close();
			outd.close();
		} catch (Exception e1) {
			WorkStationDriver.xmlLog.append("CRDMenuTasks::Display_JarEntry:"+e1.toString());
			e1.printStackTrace();
			return;
		}
	}

	public String getFileName() {
		return fileName;
	}
}

