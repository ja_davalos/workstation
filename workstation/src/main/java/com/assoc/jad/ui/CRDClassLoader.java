package com.assoc.jad.ui;

import java.io.BufferedInputStream;
import java.io.File;
import java.lang.reflect.Method;
import java.util.Enumeration;
import java.util.jar.JarFile;

import javax.swing.JTextArea;

import com.assoc.jad.common.ViewBinary;

public class CRDClassLoader  extends ClassLoader  implements Runnable { 
	public String entry="";
	public String jarlocation="";
	public File classfile;
	private JTextArea Logger;
	private CRDTools crdtools = new CRDTools();
	private String classPath;
	
	public CRDClassLoader(JTextArea Logger) {
		this.Logger = Logger;
	}
    public Class<?> findClass(String name) {
		String wrkname = name.replaceAll("\\.","/")+".class";
		boolean found = false;
		JarFile jarfile = null;
		byte[] bytes = null;
		
		String[] jars = classPath.split(System.getProperty("path.separator"));
		try {
			for (int ii = 0; ii < jars.length; ii++) {
				if (jars[ii].indexOf(".jar") == -1) continue;
				jarfile = new JarFile(jars[ii]);
				Enumeration<?> jarentries = jarfile.entries();
				String entry = "";
				for (; jarentries.hasMoreElements() ;) {
					entry = jarentries.nextElement().toString();
					if (entry.equals(wrkname)) {
						ii = jars.length;
						found = true;
						break;
					}
				}
			}
			if (!found) return null;
			BufferedInputStream src_jar = new BufferedInputStream(jarfile.getInputStream(jarfile.getEntry(wrkname)));
			bytes = new byte[src_jar.available()];
			src_jar.read(bytes,0,bytes.length);
			src_jar.close();
		} catch (Exception e) {
			return null;
		}
        return defineClass(name, bytes, 0, bytes.length);
    }
	private String getDependentJars() {
		String classPath = System.getProperty("java.class.path");
		int ndx = jarlocation.lastIndexOf(File.separator);
		String jarDir = jarlocation.substring(0,ndx);
		File wrkdir = new File(jarDir);
		if (!wrkdir.isDirectory()) return classPath;
		
		String[] jars = wrkdir.list();
		String wrkjar = "";
		String pathSep = System.getProperty("path.separator");
		for (int ii = 0; ii < jars.length; ii++) {
			if (new File(jars + File.separator + jars[ii]).isDirectory())
				continue;
			wrkjar = jarDir + File.separator + jars[ii];
			if (classPath.indexOf(wrkjar) == -1) {
				classPath += pathSep+wrkjar;
			}
		}
		return classPath;		
	}
	private void getJarMethods() {
		String signature = "";
		try {
			ViewBinary vb = new ViewBinary();
			byte[] raw = crdtools.readFile(classfile);
			String disp = vb.ViewBldBinary(raw);
			Logger.append(disp);
			Logger.setCaretPosition(Logger.getText().length());
			Logger.requestFocus();
			entry = entry.replaceAll("/",".").replaceAll(".class","");
			Logger.append(entry+"\n");
			classPath = getDependentJars();
			Class<?> c = findLoadedClass (entry);
			if (c == null) {
			  try {
			    c = findSystemClass (entry);
			  } catch (Exception e) {
			    // Ignore these
			  }
			}
			if (c==null) c = defineClass( entry, raw, 0, raw.length );

			Method[] methods = c.getMethods();
			for (int i=0;i<methods.length;i++) {
				 Class<?>[] parm = methods[i].getParameterTypes();
					signature += methods[i].getReturnType()+" "+methods[i].getName() + " ( ";
					for (int j=0;j<parm.length;j++) {
						signature += parm[j].getName();
						if (j<parm.length-1 ) signature += ", ";
					}
					signature += ")\n";
			} 
			Logger.append(signature);
			Logger.setCaretPosition(Logger.getText().length());
		} catch (Exception e) {
			Logger.append("CRDClassLoader::getJarMethods " + e.toString());
			Logger.setCaretPosition(Logger.getText().length());
			Logger.requestFocus();
		}
	}
	
	public void run() {
		getJarMethods();
	}
}
