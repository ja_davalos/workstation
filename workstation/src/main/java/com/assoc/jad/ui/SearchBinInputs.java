package com.assoc.jad.ui;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class SearchBinInputs extends JPanel implements ActionListener {

	public static final String STRING    = "String";
	public static final String HEXSTRING = "HexString";
	public static final String INT       = "int";
	public static final String DOUBLE    = "double";
	public static final String LONG      = "long";
	
	private static final long serialVersionUID = 1L;
	
	private GridBagLayout layout = new GridBagLayout();
	private GridBagConstraints constraint = new GridBagConstraints();
	private JFrame frame = new JFrame("required Inputs");
	JTextField searchJText = new JTextField("");
	String[] primitiveTypes = { STRING, HEXSTRING,INT,DOUBLE,LONG };

	private String type;
	private String search;

	public SearchBinInputs() {
		super(new BorderLayout());
		buildPanel();
	}
	private void createLabel(String name) {
		setLayout(layout);

		JLabel label = new JLabel(name);
		constraint.gridwidth = GridBagConstraints.RELATIVE; // next-to-last
		constraint.fill = GridBagConstraints.NONE; // reset to default
		constraint.weightx = 0.0; // reset to default
		constraint.anchor = GridBagConstraints.WEST;
		layout.setConstraints(label, constraint);
		add(label);
	}
	private void makeline1(String name, JTextField fld) {
		createLabel(name);
		constraint.gridwidth = GridBagConstraints.REMAINDER; // end row
		constraint.fill = GridBagConstraints.HORIZONTAL;
		constraint.weightx = 1.0;
		layout.setConstraints(fld, constraint);
		add(fld);
	}
	private void makeline2(String name, JComboBox<String> fld) {
		createLabel(name);

		constraint.gridwidth = GridBagConstraints.REMAINDER; // end row
		constraint.fill = GridBagConstraints.HORIZONTAL;
		constraint.weightx = 1.0;
		layout.setConstraints(fld, constraint);
		add(fld);
	}
	private void makeline3(JButton fld) {

		layout.setConstraints(fld, constraint);
		constraint.anchor = GridBagConstraints.CENTER;
		fld.addActionListener(this);
		add(fld);
	}
	private void buildPanel() {

		JComboBox<String> primitiveList = new JComboBox<String>(primitiveTypes);
		primitiveList.setSelectedIndex(1);
		setType(primitiveList.getSelectedItem().toString());
		primitiveList.addActionListener(this);

		setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

		searchJText.addActionListener(this);

		makeline2("type", primitiveList);
		makeline1("value", searchJText);
		makeline3(new JButton("Continue"));
	}
	private void createAndShowGUI() {
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setOpaque(true); // content panes must be opaque
		//frame.setUndecorated(true);
		frame.setContentPane(this);
		frame.pack();
		frame.setVisible(true);
	}

	public synchronized void actionPerformed(ActionEvent e) {
		String clazzName = e.getSource().getClass().getName();
		if (clazzName == null) return;
		if (clazzName.indexOf("JTextField") != -1 ) {
			setSearch(((JTextField)e.getSource()).getText());
		} else if (clazzName.indexOf("JComboBox") != -1 ) {
			@SuppressWarnings("unchecked")
			JComboBox<String> cb = (JComboBox<String>) e.getSource();
			setType((String) cb.getSelectedItem());
		} else if (clazzName.indexOf("JButton") != -1 ) {
			notifyAll();
			frame.setVisible(false);
			setSearch(searchJText.getText());
		}
	}
	public synchronized void run() {
		createAndShowGUI();
		try {
			this.wait();
		} catch (Exception e) {}
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getSearch() {
		return search;
	}
	public void setSearch(String search) {
		this.search = search;
	}
}
