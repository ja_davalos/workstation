package com.assoc.jad.ui;

import javax.swing.*;

import java.awt.*;
import java.awt.event.*;
import java.lang.reflect.*;

import com.assoc.jad.WorkStationDriver;
import com.assoc.jad.common.*;

public class HomePanel implements Runnable  {

	public boolean OnlyPanel =  false;
	private JMenuItem textItem,subItem;
	JMenuBar mbar = new JMenuBar();
	static final String menulabel_edit  = "Edit";
	static final String menulabel_file  = "File";
	static final String menulabel_view  = "View";
	static final String itemlabel_quit  = "Exit";

	public int prevndx = 0;
	public String inputpanel = "Input Fields";
	public String msgtxt="";
	public String bordername = inputpanel;
	public String[] reqlabel;
	public String[] reqitems;
	public String[] input_data;
	public String[] input_label;
	public boolean  txfrtext;
	public String[] obj_label;
	public Object[] obj_types;
	public final static Object   lock = new Object();
	public GridBagLayout layout = new GridBagLayout();
	public JPanel textControlsPane = new JPanel();
	public JPanel contentPane = new JPanel();
	public JPanel mainPane = new JPanel();
	public JTextField[] textfield;
	public int threadID=0;
	JMenu mainmenu = null;

	public JFrame parent;
	public JTabbedPane tabbedPane;

	public HomePanel() {
	}

	private void makeline(String name,
                                    GridBagLayout gridbag,
                                    JTextField fld,
				     int lineno) {
		textControlsPane.setLayout(gridbag);

		JLabel label = new JLabel(name);
		GridBagConstraints c = new GridBagConstraints();
		c.gridwidth = GridBagConstraints.RELATIVE; //next-to-last
		c.fill = GridBagConstraints.NONE;      //reset to default
		c.weightx = 0.0;                       //reset to default
		c.anchor = GridBagConstraints.WEST;
		gridbag.setConstraints(label, c);
		textControlsPane.add(label);

		c.gridwidth = GridBagConstraints.REMAINDER;     //end row
		c.fill = GridBagConstraints.HORIZONTAL;
		c.weightx = 1.0;
		gridbag.setConstraints(fld, c);
		fld.addActionListener( new InputFldListener());
		textControlsPane.add(fld);
	}

	private void bldsubmenu(String command,int ndx2, JMenu menu) {
		int wrkndx=0;

		wrkndx = command.indexOf((int)' ');
		if (wrkndx == -1) return;
		JMenu submenu = new JMenu(command.substring(0,wrkndx));
		submenu.setName(command.substring(0,wrkndx));
		menu.add(submenu);
		for (; wrkndx < ndx2;) {
			command = command.substring(wrkndx);
			command = command.trim();
			wrkndx = command.indexOf((int)' ');
			if (wrkndx == -1) {
				wrkndx = command.length();
				ndx2 = -1;
			}
			String wrkstr = command.substring(0,wrkndx);
			if (wrkstr.equalsIgnoreCase("separator")) {
				submenu.addSeparator();
			} else {
				subItem = new JMenuItem(wrkstr);
				subItem.addActionListener(new myMenuListener());
				submenu.add(subItem);
			}
		}
	}

	private void bldmenu(String label, String command) {

		String wrkstr = "";
		String tag = "\n";
		int wrkndx=0;
		int len = command.length();
		int taglen = tag.length();
		int ndx = 0,ndx1 = 0,ndx2 =0;
		mainmenu = new JMenu(label);

		for (ndx2 = 0,ndx1=0;ndx <=len;ndx1 += ndx2) {
			ndx2 = command.substring(ndx1).indexOf(tag);
			if (ndx2 == -1 ) break;
			wrkstr = command.substring(ndx1,ndx1+ndx2);
			wrkndx = wrkstr.indexOf((int)' ');
			if (wrkndx == -1) wrkndx = ndx2;
			ndx1 += taglen;
			if (wrkndx < ndx2) bldsubmenu(wrkstr,ndx2,mainmenu);
			else {
				if (wrkstr.equalsIgnoreCase("separator")) {
					mainmenu.addSeparator();
				} else {
					textItem = new JMenuItem(wrkstr);
					textItem.addActionListener(new myMenuListener());
					mainmenu.add(textItem);
				}
			}
		}

		mbar.add(mainmenu);
		parent.setJMenuBar(mbar);
	}

	public synchronized void sysmenu() {
		if ((reqlabel == null) || (reqitems == null)) return;

		bldmenu(menulabel_file,itemlabel_quit+"\n");
		bldmenu(menulabel_edit,"view VTop VBottom VNext NPrev VFind VEnd VBinary VText\n");

		if ((reqlabel != null) && (reqitems != null)) {
			for (int i = 0;i<reqlabel.length;i++) bldmenu(reqlabel[i],reqitems[i]);
		}
	}

	private synchronized void bldpanel() {

		textControlsPane.setLayout(layout);
		if ((input_data != null ) && (input_label != null)) {
			textfield = new JTextField[input_data.length];
			for (int i = 0;i<input_data.length;i++) {
				if (input_label[i].indexOf("Password") >= 0)
					textfield[i] = new JPasswordField(input_data[i],10);
				else {
					textfield[i] = new JTextField(input_data[i],10);
					textfield[i].setActionCommand(input_label[i]);
				}
				if (bordername.indexOf("INFO") > 0) textfield[i].setEditable(false);
				makeline(input_label[i], layout,textfield[i],0);
			}
		}

		if ((obj_types != null ) && (obj_label != null)) {
			GridBagConstraints c_obj = new GridBagConstraints();
			JLabel wrk_label;
			c_obj.fill = GridBagConstraints.VERTICAL;
			c_obj.anchor = GridBagConstraints.WEST;
			c_obj.weightx = 1.0;
			for (int i = 0;i<obj_types.length;i++) {
				wrk_label = new JLabel(obj_label[i]);
				c_obj.gridwidth = GridBagConstraints.RELATIVE;
				layout.setConstraints(wrk_label, c_obj);
				textControlsPane.add(wrk_label);
//				c_obj.gridwidth = GridBagConstraints.REMAINDER;     //end row
				layout.setConstraints((java.awt.Component)obj_types[i], c_obj);
				textControlsPane.add((java.awt.Component)obj_types[i]);
			}
		}

        mainPane.setBorder(
		BorderFactory.createCompoundBorder(
			BorderFactory.createTitledBorder(bordername),
			BorderFactory.createEmptyBorder(5,5,5,5)));
		GridBagConstraints c = new GridBagConstraints();
 		JButton okbutton = new JButton("ok");
		okbutton.addActionListener(new InputFldListener());
		c.anchor = GridBagConstraints.SOUTH;
		layout.setConstraints(okbutton, c);
		textControlsPane.add(new JLabel(""));
		textControlsPane.add(okbutton);
		if (msgtxt.length() > 0) {
		    JTextField status = new JTextField(msgtxt);
			status.setEditable(false);
			textControlsPane.add(status);
		}

		mainPane.add(textControlsPane);
		parent.setVisible(true);
    }

	public void run() {
		bldpanel();
	    if (tabbedPane.getTabCount() == 0) tabbedPane.addTab(bordername, mainPane);
	    else tabbedPane.setSelectedIndex(0);
	}

	public static void main(String[] args) {
	}

	class myMenuListener implements ActionListener {
		public synchronized void actionPerformed(ActionEvent ev) {
			String command = (String)ev.getActionCommand();
			String commandGroup = null;
			if (ev.getModifiers() == ActionEvent.META_MASK) {
				JOptionPane.showMessageDialog(WorkStationDriver.parentFrame,"command","command to display",JOptionPane.INFORMATION_MESSAGE);
				return;
			}

			if (command.equals(itemlabel_quit)) {
				System.exit(0);
			}
			JMenuItem item = (JMenuItem)ev.getSource();
			JPopupMenu wrkmenu = (JPopupMenu)item.getParent();
			if (wrkmenu.getInvoker().getName() != null ) commandGroup = wrkmenu.getInvoker().getName();
			
			if (wrkmenu.getInvoker().getName() != null &&
			   (wrkmenu.getInvoker().getName().equalsIgnoreCase("MTsInstalled") ||
//				wrkmenu.getInvoker().getName().equalsIgnoreCase("fonts") ||
				wrkmenu.getInvoker().getName().equalsIgnoreCase("size") ||
				wrkmenu.getInvoker().getName().equalsIgnoreCase("styles") ||
				wrkmenu.getInvoker().getName().equalsIgnoreCase("helpScratchPad"))
				) {
				commandGroup = command;
				command = wrkmenu.getInvoker().getName();
			}
			System.out.println("CRDPanel::myMenuListener: command="+command+" commandGroup="+commandGroup);
			new Thread(WorkStationDriver.commandObjs.get(command), command+threadID).start();
			threadID++;
		}
	}

	class InputFldListener implements ActionListener {
		public synchronized void actionPerformed(ActionEvent ev) {
			if (input_data != null )
				for (int i = 0;i<input_data.length;i++)
					input_data[i] = textfield[i].getText().trim();

			if (obj_types != null ) {
				for (int i = 0;i<obj_types.length;i++) {
					Class<? extends Object> c = obj_types[i].getClass();
					 @SuppressWarnings("rawtypes")
					Class[] parameterTypes = new Class[] {};
					 Method method;
					Object[] arguments = new Object[] {};
					try {
						method = c.getMethod("getText", parameterTypes);
						obj_label[i] = (String) method.invoke(obj_types[i],arguments);
					} catch (NoSuchMethodException e) {
						try {
							method = c.getMethod("getSelectedItem", parameterTypes);
							obj_label[i] = (String) method.invoke(obj_types[i],arguments);
							CommonGlobals.setMessage(obj_label[i]);
						} catch (Exception e1) {
							System.out.println("CRDPanel.InputFldListener.getSelectedItem:"+e);
						}
					} catch (Exception e) {
					   System.out.println("CRDPanel.InputFldListener:"+e);
					}
				}
			}
			//method = "setInput";
		}
	}
}
