package com.assoc.jad.ui;

import javax.swing.*;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeSelectionModel;

import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.*;
import java.net.URL;

import com.assoc.jad.WorkStationDriver;
import com.assoc.jad.common.*;

public class CRDPanel implements Runnable  {

	private  static final String menulabel_edit  = "Edit";
	private static final String menulabel_file  = "File";
	private static final String itemlabel_quit  = "Exit";

	public String[] reqlabel;
	public String[] reqitems;
	public boolean OnlyPanel =  false;
	public JPanel contentPane = new JPanel();
	public JTabbedPane tabbedPane =  new JTabbedPane();

	private String[] input_data;
	private String[] obj_label;
	private Object[] obj_types;
	private JPanel textControlsPane = new JPanel();
	private JPanel mainPane = new JPanel();
	private JTextField[] textfield;
	private int threadID=0;
	private JMenu mainmenu = null;
	private JMenuBar mbar = new JMenuBar();

	private JEditorPane htmlPane;
	private JTree tree = null;
	private String htmlFile = "/help/workStation.html";
	private String htmlTemplate = "/help/template.html";

	private void bldsubmenu(String command,int ndx2, JMenu menu) {
		int wrkndx=0;
		JMenuItem subItem;
		wrkndx = command.indexOf((int)' ');
		if (wrkndx == -1) return;
		JMenu submenu = new JMenu(command.substring(0,wrkndx));
		submenu.setName(command.substring(0,wrkndx));
		menu.add(submenu);
		for (; wrkndx < ndx2;) {
			command = command.substring(wrkndx);
			command = command.trim();
			wrkndx = command.indexOf((int)' ');
			if (wrkndx == -1) {
				wrkndx = command.length();
				ndx2 = -1;
			}
			String wrkstr = command.substring(0,wrkndx);
			if (wrkstr.equalsIgnoreCase("separator")) {
				submenu.addSeparator();
			} else {
				subItem = new JMenuItem(wrkstr);
				subItem.addActionListener(new myMenuListener());
				submenu.add(subItem);
			}
		}
	}

	private void bldmenu(String label, String command) {

		String wrkstr = "";
		String tag = "\n";
		int wrkndx=0;
		int len = command.length();
		int taglen = tag.length();
		int ndx = 0,ndx1 = 0,ndx2 =0;
		mainmenu = new JMenu(label);
		JMenuItem textItem;

		for (ndx2 = 0,ndx1=0;ndx <=len;ndx1 += ndx2) {
			ndx2 = command.substring(ndx1).indexOf(tag);
			if (ndx2 == -1 ) break;
			wrkstr = command.substring(ndx1,ndx1+ndx2);
			wrkndx = wrkstr.indexOf((int)' ');
			if (wrkndx == -1) wrkndx = ndx2;
			ndx1 += taglen;
			if (wrkndx < ndx2) bldsubmenu(wrkstr,ndx2,mainmenu);
			else {
				if (wrkstr.equalsIgnoreCase("separator")) {
					mainmenu.addSeparator();
				} else {
					textItem = new JMenuItem(wrkstr);
					textItem.addActionListener(new myMenuListener());
					mainmenu.add(textItem);
				}
			}
		}

		mbar.add(mainmenu);
		WorkStationDriver.parentFrame.setJMenuBar(mbar);
	}

	public void sysmenu() {
		if ((reqlabel == null) || (reqitems == null)) return;

		bldmenu(menulabel_file,itemlabel_quit+"\n");
		bldmenu(menulabel_edit,"view VTop VBottom VNext NPrev VFind VEnd VBinary VText\n");

		if ((reqlabel != null) && (reqitems != null)) {
			for (int i = 0;i<reqlabel.length;i++) bldmenu(reqlabel[i],reqitems[i]);
		}
	}

	private void createNodes(DefaultMutableTreeNode top, String groupTitle, String commands) {
        DefaultMutableTreeNode category = null;
        DefaultMutableTreeNode subCategory = null;
 
        category = new DefaultMutableTreeNode(groupTitle);
        top.add(category);

        String[] categories = commands.split("\n");
        
        for (int i =0;i<categories.length;i++) {
        	String[] cmds = categories[i].split(" ");
        	subCategory = new DefaultMutableTreeNode(cmds[0]);
        	category.add(subCategory);
        	for (int j=1;j<cmds.length;j++) {
        		subCategory.add(new DefaultMutableTreeNode(cmds[j]));
        	}
        	
        }
	}
	private void bldHtmlFile(String command) {
		int ndx = htmlTemplate.lastIndexOf('/');
		String helpFile = htmlTemplate.substring(0, ++ndx)+command+".html";
		URL url = getClass().getResource(helpFile);
		if (url == null) {
			url = getClass().getResource(htmlTemplate);
			
			String cmdFileName = url.getFile().replaceFirst(htmlTemplate, helpFile);
			File file = new File(cmdFileName);
			BufferedWriter writer = null;
			BufferedReader reader = null;
			StringBuilder sb = new StringBuilder();
	
		    try {
				reader = new BufferedReader(new InputStreamReader(url.openStream()));
				writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
				reader.lines().forEach(line -> sb.append(line).append(System.lineSeparator()));
				writer.write(sb.toString());
				writer.close();
				url = getClass().getResource(helpFile);
			} catch (Exception e1) {
				e1.printStackTrace();
			} finally {
				if (writer != null) 
					try { writer.close();} catch (IOException e1) {}
				if (reader != null) 
					try { reader.close();} catch (IOException e1) {}
				
			}
		}
	    displayURL(url);
	}
    private void displayURL(URL url) {
        try {
            if (url != null) {
                htmlPane.setPage(url);
                htmlPane.repaint();
            } else { //null url
            	htmlPane.setText("File Not Found");
            }
        } catch (IOException e) {
            System.err.println("Attempted to read a bad URL: " + url);
        }
    }
 	private void bldFrontPage() {
		mainPane.setLayout(new GridLayout(1,0));
        
        DefaultMutableTreeNode top = new DefaultMutableTreeNode("WorkStation Commands");
		if ((reqlabel != null) && (reqitems != null)) {
			for (int i = 0;i<reqlabel.length;i++) createNodes(top,reqlabel[i],reqitems[i]);
		}
        tree = new JTree(top);
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.addTreeSelectionListener(new MyTreeSelectionListener());
        tree.addMouseListener(new MyMouseListener());
        JScrollPane treeView = new JScrollPane(tree);
        
        htmlPane = new JEditorPane();
        htmlPane.setEditable(true);
        htmlPane.addFocusListener(new MyFocusListener());
        displayURL(getClass().getResource(htmlFile));

        JScrollPane htmlView = new JScrollPane(htmlPane);
 
        //Add the scroll panes to a split pane.
        JSplitPane splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT);
        splitPane.setTopComponent(treeView);
        splitPane.setBottomComponent(htmlView);
 
        Dimension minimumSize = new Dimension(100, 50);
        htmlView.setMinimumSize(minimumSize);
        treeView.setMinimumSize(minimumSize);
        splitPane.setDividerLocation(200); 
        splitPane.setPreferredSize(new Dimension(500, 300));
 
        //Add the split pane to this panel.
        mainPane.add(splitPane);

	}

	private void bldpanel() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {}
         mainPane.setBorder(
        		BorderFactory.createCompoundBorder(BorderFactory.createTitledBorder("Front Page"),BorderFactory.createEmptyBorder(5,5,5,5)));
        bldFrontPage();
    }

	public synchronized void run() {
		textControlsPane.removeAll();
		if (!OnlyPanel) {
			WorkStationDriver.parentFrame.setJMenuBar(null);
			mbar = new JMenuBar();
			sysmenu();
		}
		bldpanel();
	    if (tabbedPane.getTabCount() == 0) tabbedPane.addTab("Main Menu", mainPane);
	    else tabbedPane.setSelectedIndex(0);
	}
/*
 * inner listener classes
 */
	class myMenuListener implements ActionListener {
		public synchronized void actionPerformed(ActionEvent ev) {
			String command = (String)ev.getActionCommand();
			String commandGroup = null;
			if (ev.getModifiers() == ActionEvent.META_MASK) {
				JOptionPane.showMessageDialog(WorkStationDriver.parentFrame,"command","command to display",JOptionPane.INFORMATION_MESSAGE);
				return;
			}

			if (command.equals(itemlabel_quit)) {
				System.exit(0);
			}
			JMenuItem item = (JMenuItem)ev.getSource();
			JPopupMenu wrkmenu = (JPopupMenu)item.getParent();
			if (wrkmenu.getInvoker().getName() != null ) commandGroup = wrkmenu.getInvoker().getName();
			
			if (wrkmenu.getInvoker().getName() != null &&
			   (wrkmenu.getInvoker().getName().equalsIgnoreCase("MTsInstalled") ||
//				wrkmenu.getInvoker().getName().equalsIgnoreCase("fonts") ||
				wrkmenu.getInvoker().getName().equalsIgnoreCase("size") ||
				wrkmenu.getInvoker().getName().equalsIgnoreCase("styles") ||
				wrkmenu.getInvoker().getName().equalsIgnoreCase("helpScratchPad"))
				) {
				commandGroup = command;
				command = wrkmenu.getInvoker().getName();
			}
			System.out.println("CRDPanel::myMenuListener: command="+command+" commandGroup="+commandGroup);
			new Thread(WorkStationDriver.commandObjs.get(command), command+threadID).start();
			threadID++;
		}
	}
	class InputFldListener implements ActionListener {
		public synchronized void actionPerformed(ActionEvent ev) {
			if (input_data != null )
				for (int i = 0;i<input_data.length;i++)
					input_data[i] = textfield[i].getText().trim();

			if (obj_types != null ) {
				for (int i = 0;i<obj_types.length;i++) {
					Class<? extends Object> c = obj_types[i].getClass();
					 @SuppressWarnings("rawtypes")
					Class[] parameterTypes = new Class[] {};
					 Method method;
					Object[] arguments = new Object[] {};
					try {
						method = c.getMethod("getText", parameterTypes);
						obj_label[i] = (String) method.invoke(obj_types[i],arguments);
					} catch (NoSuchMethodException e) {
						try {
							method = c.getMethod("getSelectedItem", parameterTypes);
							obj_label[i] = (String) method.invoke(obj_types[i],arguments);
							CommonGlobals.setMessage(obj_label[i]);
						} catch (Exception e1) {
							System.out.println("CRDPanel.InputFldListener.getSelectedItem:"+e);
						}
					} catch (Exception e) {
					   System.out.println("CRDPanel.InputFldListener:"+e);
					}
				}
			}
			//method = "setInput";
		}
	}
	class MyTreeSelectionListener implements TreeSelectionListener {

		public void valueChanged(TreeSelectionEvent e) {
	        DefaultMutableTreeNode node = (DefaultMutableTreeNode)tree.getLastSelectedPathComponent();
			if (node != null && node.isLeaf()) 
				bldHtmlFile(node.toString());
		}
		
	}
	class MyFocusListener implements FocusListener {

		public void focusGained(FocusEvent e) {			
		}
		public void focusLost(FocusEvent e) {
			JEditorPane editorPane = (JEditorPane) e.getSource();
			URL url = htmlPane.getPage();
			if (url == null) return;
			File file = new File(url.getFile());
			if (!file.canWrite()) return;
			
			BufferedWriter writer = null;
		    try {
				writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file)));
				writer.write(editorPane.getText().replaceAll("&lt;", "<").replaceAll("&gt;", ">"));
			} catch (Exception e1) {
				e1.printStackTrace();
			} finally {
				if (writer != null) {
					try { writer.close();} catch (IOException e1) {}
				}
			}
		}
		
	}
	class MyMouseListener implements MouseListener {

		public void mouseClicked(MouseEvent e) {
	        DefaultMutableTreeNode node = (DefaultMutableTreeNode)tree.getLastSelectedPathComponent();
			if (node != null && node.isLeaf() && e.getClickCount() == 2) {
				String command = node.toString();
				new Thread(WorkStationDriver.commandObjs.get(command), command).start();
			}			
		}
		public void mousePressed(MouseEvent e) {			
		}
		public void mouseReleased(MouseEvent e) {
		}
		public void mouseEntered(MouseEvent e) {
		}

		public void mouseExited(MouseEvent e) {
		}
	}
}
