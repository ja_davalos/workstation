package com.assoc.jad.ui;

import javax.print.PrintServiceLookup;

public class javaCmd implements java.io.Serializable, Runnable {
	static final long serialVersionUID = 7;
	public String outstr = "";
	
	javaCmd() {
	}
    public void checkPrinters() {

        javax.print.PrintService [] matchingServices = PrintServiceLookup.lookupPrintServices( null, null);
        if (matchingServices.length == 0) {
            outstr += "No printers installed on the server running dispatcher.";
        }
        outstr += "Found " + matchingServices.length + "printers.";

        StringBuffer knownPrinters = new StringBuffer("\n");
        for (int i = 0; i < matchingServices.length; i++) {
            String knownPrinterName = matchingServices[i].getName();
            knownPrinters.append( "'");
            knownPrinters.append( knownPrinterName);
            knownPrinters.append( "' \n");
        }
        outstr += knownPrinters.toString();
    }
 	public  void run() {
 		outstr = "";
		try {
			 SecurityManager security = System.getSecurityManager();
		     if (security != null) {
		    	 security.checkPrintJobAccess();
		     }
		     checkPrinters();
		     outstr += "javaCmd::run: has ended successfully\n";
		} catch (Exception e) {
			System.err.println("javaCmd::run: "+e.toString());
			StackTraceElement[] st = e.getStackTrace();
			for (int ii=0;ii<st.length;ii++) outstr += st[ii].toString()+"\n";
		}
	}

}
